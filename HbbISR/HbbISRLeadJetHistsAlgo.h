#ifndef HbbISR_HbbISRLeadJetHistsAlgo_H
#define HbbISR_HbbISRLeadJetHistsAlgo_H

// algorithm wrapper
#include <HbbISR/HbbISRHistsBaseAlgo.h>

class HbbISRLeadJetHistsAlgo : public HbbISRHistsBaseAlgo
{
public:

protected:
  virtual void initHcandCutflow();
  virtual void initBtagCutflow();
  virtual bool doHcandCutflow();
  virtual bool doBtagCutflow();

public:
  // this is a standard constructor
  HbbISRLeadJetHistsAlgo ();

private:
  //
  // cutflow
  int m_cf_nfatjets;

  // this is needed to distribute the algorithm to the workers
  ClassDef(HbbISRLeadJetHistsAlgo, 1);
};

#endif // HbbISR_HbbISRLeadJetHistsAlgo_H
