#include "HbbISR/HbbISRHelperClasses.h"

namespace HbbISRHelperClasses
{

  void HbbISRInfoSwitch::initialize()
  {
    m_mass    = has_exact("mass");
    m_pt      = has_exact("pt");
    m_truthtag= has_exact("truthtag");
  }


  FatJetSorter FatJetSorter::ptsort(Mode::Pt);
  FatJetSorter FatJetSorter::d2sort(Mode::D2);

  FatJetSorter::FatJetSorter(Mode mode)
    : m_mode(mode)
  { }

  bool FatJetSorter::operator()(const xAH::FatJet* a, const xAH::FatJet* b)
    {
      switch(m_mode)
	{
	default:
	case Pt:
	  return a->p4.Pt() > b->p4.Pt();
	  break;
	case D2:
	  return a->D2 > b->D2;
	  break;
	}
    }
}
