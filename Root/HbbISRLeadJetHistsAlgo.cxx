#include <HbbISR/HbbISRLeadJetHistsAlgo.h>

#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>

#include <AsgTools/MessageCheck.h>

#include <xAODAnaHelpers/HelperFunctions.h>
#include <xAODAnaHelpers/HelperClasses.h>

#include <algorithm>

// this is needed to distribute the algorithm to the workers
ClassImp(HbbISRLeadJetHistsAlgo)

HbbISRLeadJetHistsAlgo :: HbbISRLeadJetHistsAlgo ()
: HbbISRHistsBaseAlgo()
{ }

void HbbISRLeadJetHistsAlgo :: initHcandCutflow ()
{
  m_cf_nfatjets = m_cutflow->addCut("NFatJets");
}

void HbbISRLeadJetHistsAlgo :: initBtagCutflow()
{ }

bool HbbISRLeadJetHistsAlgo :: doHcandCutflow ()
{
  uint nfatjets = m_event->fatjets();

  if(nfatjets < 1)
    {
      ANA_MSG_DEBUG(" Fail NFatJets with " << nfatjets);
      return false;
    }
  m_cutflow->execute(m_cf_nfatjets,m_eventWeight);

  //
  // Choose h-cand
  m_Hcand=m_event->fatjet(0);

  return true;
}

bool HbbISRLeadJetHistsAlgo :: doBtagCutflow()
{
  return true;
}
