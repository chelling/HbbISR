#include <HbbISR/HbbISRTJetHistsAlgo.h>

#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>

#include <AsgTools/MessageCheck.h>

#include <xAODAnaHelpers/HelperFunctions.h>
#include <xAODAnaHelpers/HelperClasses.h>

#include <algorithm>

// this is needed to distribute the algorithm to the workers
ClassImp(HbbISRTJetHistsAlgo)

HbbISRTJetHistsAlgo :: HbbISRTJetHistsAlgo ()
: HbbISRHistsBaseAlgo(),
  m_trkjet1PtCut(0),
  m_bTagWP(xAH::Jet::BTaggerOP::None),
  m_looseNoTight(false),
  m_nBTagsCut(0),
  m_nBTagsCutExact(false)
{ }

void HbbISRTJetHistsAlgo :: initHcandCutflow ()
{
  m_cf_nfatjets       = m_cutflow->addCut("NFatJets");
  if(m_skipDoubleTrkJet) m_cf_noDoubleTrkJet = m_cutflow->addCut("NoDoubleTrkJet");
  m_cf_hcand          = m_cutflow->addCut("ZprimeCand");
}

void HbbISRTJetHistsAlgo :: initBtagCutflow ()
{
  if(m_trkjet1PtCut>0)                  m_cf_trkjet1pt = m_cutflow->addCut("SubTrkJetPt");
  if(m_looseNoTight)                    m_cf_notight   = m_cutflow->addCut("NoTight");
  if(m_nBTagsCut>0 || m_nBTagsCutExact) m_cf_nbtag     = m_cutflow->addCut("NBTag");
}

bool HbbISRTJetHistsAlgo :: doHcandCutflow ()
{
  uint nfatjets = m_event->fatjets();

  if(nfatjets < 1)
    {
      ANA_MSG_DEBUG(" Fail NFatJets with " << nfatjets);
      return false;
    }
  m_cutflow->execute(m_cf_nfatjets,m_eventWeight);

  //
  // Run the preselection
  std::vector<const xAH::FatJet *> fatjetcandidates;

  // Look for two sub track jets
  for(uint i=0;i<m_event->fatjets();i++)
    {
      const xAH::FatJet *fatjet=m_event->fatjet(i);
      std::vector<xAH::Jet> trkJets=fatjet->trkJets.at(m_trkJet);
      if(trkJets.size()<2) continue;

      if(!m_skipBoostCut)
	{
	  float boostR=2*fatjet->p4.M()/fatjet->p4.Pt();
	  if(boostR>1) continue;
	}

      if(!m_skipVRContainCut)
	{
	  const xAH::Jet *tjet0=&trkJets.at(0);
	  const xAH::Jet *tjet1=&trkJets.at(1);
	  float R0=std::max(0.02, std::min(0.4, 30/tjet0->p4.Pt()));
	  float R1=std::max(0.02, std::min(0.4, 30/tjet1->p4.Pt()));
	  float minR=std::min(R0,R1);
	  //float maxR=std::max(R0,R1);
	  float dR=tjet0->p4.DeltaR(tjet1->p4);

	  if(dR/minR<1) continue;
	}

      fatjetcandidates.push_back(fatjet);
    }

  if(m_skipDoubleTrkJet)
    {
      if(fatjetcandidates.size()>m_HcandIdx)
	{
	  ANA_MSG_DEBUG(" Skip Z' with two track jets.");
	  return false;
	}
      m_cutflow->execute(m_cf_noDoubleTrkJet, m_eventWeight);
    }

  // Look for one sub track jets, if no two track jets exist
  if(m_allowSingleTrkJet && fatjetcandidates.size()<=m_HcandIdx)
    {
      for(uint i=0;i<m_event->fatjets();i++)
	{
	  const xAH::FatJet *fatjet=m_event->fatjet(i);
	  std::vector<xAH::Jet> trkJets=fatjet->trkJets.at(m_trkJet);
	  if(trkJets.size()!=1) continue;

	  float boostR=2*fatjet->p4.M()/fatjet->p4.Pt();
	  if(boostR>1) continue;

	  fatjetcandidates.push_back(fatjet);
	}
    }

  // Check for candidates
  if(fatjetcandidates.size()<=m_HcandIdx)
    {
      ANA_MSG_DEBUG(" Fail Z' candidate cut");
      return false;
    }
  m_cutflow->execute(m_cf_hcand, m_eventWeight);

  // Sort by something and pick Hcand
  if(m_sortD2)
    {
      std::sort(fatjetcandidates.begin(), fatjetcandidates.end(), HbbISRHelperClasses::FatJetSorter::d2sort); 
      std::reverse(fatjetcandidates.begin(), fatjetcandidates.end());
    }
  m_Hcand=fatjetcandidates.at(m_HcandIdx);
  if(fatjetcandidates.size()>1) m_nonHcand=fatjetcandidates.at((m_HcandIdx==0)?1:0);

  return true;
}

bool HbbISRTJetHistsAlgo :: doBtagCutflow ()
{
  std::vector<xAH::Jet> trkJets=m_Hcand->trkJets.at(m_trkJet);

  // Track jet pT cuts
  if(m_trkjet1PtCut>0)
    {
      if(trkJets.size()<2 || trkJets.at(1).p4.Pt()<m_trkjet1PtCut)
	{
	  ANA_MSG_DEBUG(" Fail subleading track jet pT cut with " << trkJets.at(1).p4.Pt());
	  return false;
	}
      m_cutflow->execute(m_cf_trkjet1pt,m_eventWeight);
    }

  //
  // Find btagged jet
  if(m_nBTagsCut>0 || m_nBTagsCutExact)
    {
      bool supportedWP=(m_bTagWP!=xAH::Jet::DL1rnn_FixedCutBEff_60    && m_bTagWP!=xAH::Jet::DL1rnn_FixedCutBEff_70    && m_bTagWP!=xAH::Jet::DL1rnn_FixedCutBEff_77    && m_bTagWP!=xAH::Jet::DL1rnn_FixedCutBEff_85 &&
			m_bTagWP!=xAH::Jet::DL1rnn_HybBEff_60         && m_bTagWP!=xAH::Jet::DL1rnn_HybBEff_70         && m_bTagWP!=xAH::Jet::DL1rnn_HybBEff_77         && m_bTagWP!=xAH::Jet::DL1rnn_HybBEff_85 &&
			m_bTagWP!=xAH::Jet::DL1mu_FixedCutBEff_60     && m_bTagWP!=xAH::Jet::DL1mu_FixedCutBEff_70     && m_bTagWP!=xAH::Jet::DL1mu_FixedCutBEff_77     && m_bTagWP!=xAH::Jet::DL1mu_FixedCutBEff_85 &&
			m_bTagWP!=xAH::Jet::DL1mu_HybBEff_60          && m_bTagWP!=xAH::Jet::DL1mu_HybBEff_70          && m_bTagWP!=xAH::Jet::DL1mu_HybBEff_77          && m_bTagWP!=xAH::Jet::DL1mu_HybBEff_85 &&
			m_bTagWP!=xAH::Jet::MV2c10rnn_FixedCutBEff_60 && m_bTagWP!=xAH::Jet::MV2c10rnn_FixedCutBEff_70 && m_bTagWP!=xAH::Jet::MV2c10rnn_FixedCutBEff_77 && m_bTagWP!=xAH::Jet::MV2c10rnn_FixedCutBEff_85 &&
			m_bTagWP!=xAH::Jet::MV2c10rnn_HybBEff_60      && m_bTagWP!=xAH::Jet::MV2c10rnn_HybBEff_70      && m_bTagWP!=xAH::Jet::MV2c10rnn_HybBEff_77      && m_bTagWP!=xAH::Jet::MV2c10rnn_HybBEff_85 &&
			m_bTagWP!=xAH::Jet::MV2c10mu_FixedCutBEff_60  && m_bTagWP!=xAH::Jet::MV2c10mu_FixedCutBEff_70  && m_bTagWP!=xAH::Jet::MV2c10mu_FixedCutBEff_77  && m_bTagWP!=xAH::Jet::MV2c10mu_FixedCutBEff_85 &&
			m_bTagWP!=xAH::Jet::MV2c10mu_HybBEff_60       && m_bTagWP!=xAH::Jet::MV2c10mu_HybBEff_70       && m_bTagWP!=xAH::Jet::MV2c10mu_HybBEff_77       && m_bTagWP!=xAH::Jet::MV2c10mu_HybBEff_85);

      unsigned int maxTrkJets=0;
      unsigned int trkJetWeight=0;
      if(trkJets.size()==1)
	{
	  maxTrkJets=1;
	  trkJetWeight=2;
	}
      else
	{
	  maxTrkJets=2;
	  trkJetWeight=1;
	}

      unsigned int nbtags=0;
      bool tight=false;
      for(uint i=0; i<maxTrkJets; i++)
	{
	  const xAH::Jet& trkJet=trkJets.at(i);
	  if(btag(&trkJet)) nbtags+=trkJetWeight;
	  m_eventWeight*=(m_bTagWP!=xAH::Jet::None && supportedWP)?trkJet.SF_btag(m_bTagWP)[0]:1.;
	  if(m_looseNoTight && trkJet.is_btag(m_bTagWP))
	    {
	      tight=true;
	      break;
	    }
	}
      if(m_looseNoTight && tight)
	{
	  ANA_MSG_DEBUG(" Fail no tight");
	  return false;
	}
      m_cutflow->execute(m_cf_notight, m_eventWeight);

      if((m_nBTagsCutExact)?(nbtags!=m_nBTagsCut):(nbtags<m_nBTagsCut))
	{
	  ANA_MSG_DEBUG(" Fail NBTag with " << nbtags);
	  return false;
	}
      m_cutflow->execute(m_cf_nbtag,m_eventWeight);
    }

  return true;
}

bool HbbISRTJetHistsAlgo::btag(const xAH::Jet* trkjet) const
{
  if(m_bTagWP==xAH::Jet::None) return true;
  bool tight=trkjet->is_btag(m_bTagWP);
  if(m_looseNoTight) return trkjet->is_btag(xAH::Jet::MV2c10_FixedCutBEff_85) && !tight;
  return tight;
}
