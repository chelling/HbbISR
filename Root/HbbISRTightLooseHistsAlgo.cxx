#include <HbbISR/HbbISRTightLooseHistsAlgo.h>

#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>

#include <AsgTools/MessageCheck.h>

#include <xAODAnaHelpers/HelperFunctions.h>
#include <xAODAnaHelpers/HelperClasses.h>

#include <algorithm>

// this is needed to distribute the algorithm to the workers
ClassImp(HbbISRTightLooseHistsAlgo)

HbbISRTightLooseHistsAlgo :: HbbISRTightLooseHistsAlgo ()
: HbbISRHistsBaseAlgo(),
  m_bTagWPtight(xAH::Jet::BTaggerOP::None),
  m_bTagWPloose(xAH::Jet::BTaggerOP::None),
  m_tightBtag0(0),
  m_tightBtag1(0),
  m_looseBtag0(0),
  m_looseBtag1(0)
{ }

void HbbISRTightLooseHistsAlgo :: initHcandCutflow ()
{
  m_cf_nfatjets = m_cutflow->addCut("NFatJets");
  m_cf_hcand    = m_cutflow->addCut("ZprimeCand");
}

void HbbISRTightLooseHistsAlgo :: initBtagCutflow ()
{
  if(m_tightBtag0!=0 || m_looseBtag0!=0) m_cf_btag0=m_cutflow->addCut("Btag0");
  if(m_tightBtag1!=0 || m_looseBtag1!=0) m_cf_btag1=m_cutflow->addCut("Btag1");
}

bool HbbISRTightLooseHistsAlgo :: doHcandCutflow ()
{
  uint nfatjets = m_event->fatjets();

  if(nfatjets < 1)
    {
      ANA_MSG_DEBUG(" Fail NFatJets with " << nfatjets);
      return false;
    }
  m_cutflow->execute(m_cf_nfatjets,m_eventWeight);

  //
  // Run the preselection
  std::vector<const xAH::FatJet *> fatjetcandidates;
  m_Hcand=nullptr;
  for(uint i=0;i<m_event->fatjets();i++)
    {
      const xAH::FatJet *fatjet=m_event->fatjet(i);
      std::vector<xAH::Jet> trkJets=fatjet->trkJets.at(m_trkJet);
      if(trkJets.size()<m_nTrackJetsCut) continue;

      float boostR=2*fatjet->p4.M()/fatjet->p4.Pt();
      if(boostR>1) continue;

      const xAH::Jet *tjet0=&trkJets.at(0);
      const xAH::Jet *tjet1=&trkJets.at(1);
      float R0=std::max(0.02, std::min(0.4, 30/tjet0->p4.Pt()));
      float R1=std::max(0.02, std::min(0.4, 30/tjet1->p4.Pt()));
      float minR=std::min(R0,R1);
      float maxR=std::max(R0,R1);
      float dR=tjet0->p4.DeltaR(tjet1->p4);

      if(dR/minR<1) continue;

      fatjetcandidates.push_back(fatjet);
    }
  
  // Sort by something
  if(m_sortD2)
    {
      std::sort(fatjetcandidates.begin(), fatjetcandidates.end(), HbbISRHelperClasses::FatJetSorter::d2sort); 
      std::reverse(fatjetcandidates.begin(), fatjetcandidates.end());
    }

  // Check for candidates
  if(fatjetcandidates.size()==0)
    {
      ANA_MSG_DEBUG(" Fail Z' candidate cut");
      return false;
    }
  m_cutflow->execute(m_cf_hcand, m_eventWeight);

  m_Hcand=fatjetcandidates.at(0);

  return true;
}

bool HbbISRTightLooseHistsAlgo :: doBtagCutflow ()
{
  //
  // Find btagged jet
  std::vector<xAH::Jet> trkJets=m_Hcand->trkJets.at(m_trkJet);

  if(m_tightBtag0!=0 || m_looseBtag0!=0)
    {
      const xAH::Jet *tjet0=&trkJets.at(0);
      bool pass=true;
      if     (m_tightBtag0== 1) pass&= tjet0->is_btag(m_bTagWPtight);
      else if(m_tightBtag0==-1) pass&=!tjet0->is_btag(m_bTagWPtight);

      if     (m_looseBtag0== 1) pass&= tjet0->is_btag(m_bTagWPloose);
      else if(m_looseBtag0==-1) pass&=!tjet0->is_btag(m_bTagWPloose);

      if(!pass)
	{
	  ANA_MSG_DEBUG(" Fail btag0");
	  return false;
	}
      m_cutflow->execute(m_cf_btag0,m_eventWeight);
    }

  if(m_tightBtag1!=0 || m_looseBtag1!=0)
    {
      const xAH::Jet *tjet1=&trkJets.at(1);
      bool pass=true;
      if     (m_tightBtag1== 1) pass&= tjet1->is_btag(m_bTagWPtight);
      else if(m_tightBtag1==-1) pass&=!tjet1->is_btag(m_bTagWPtight);

      if     (m_looseBtag1== 1) pass&= tjet1->is_btag(m_bTagWPloose);
      else if(m_looseBtag1==-1) pass&=!tjet1->is_btag(m_bTagWPloose);

      if(!pass)
	{
	  ANA_MSG_DEBUG(" Fail btag1");
	  return false;
	}
      m_cutflow->execute(m_cf_btag1,m_eventWeight);
    }

  return true;
}
