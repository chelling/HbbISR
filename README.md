# Installation
To setup the environment for the first time, run the following inside your work directory. To seting up a second time, see the Setup section. If you encounter errors with xAH, make sure to use the latest AnalysisBase release.
```bash
setupATLAS
lsetup fax panda pyami
voms-proxy-init -voms atlas
git clone git@github.com:UCATLAS/xAODAnaHelpers.git
git clone ssh://git@gitlab.cern.ch:7999/atlas-phys-exotics-dijetisr/ZprimeDM.git
git clone ssh://git@gitlab.cern.ch:7999/atlas-phys-exotics-dijetisr/HbbISR.git
asetup 21.2.93,AnalysisBase,here
mkdir build && cd build
cmake ..
source ${AnalysisBase_PLATFORM}/setup.sh
make
```

# Setup
To setup an already installed environment, run the following inside your work directory.

```bash
setupATLAS
lsetup fax panda pyami
voms-proxy-init -voms atlas
asetup --restore
cd build
source ${AnalysisBase_PLATFORM}/setup.sh
```

To change the location where output is saved (default is `${pwd}`), please define the `DATADIR` environmental variable. For example, on NERSC use
```bash
export DATADIR=/global/projecta/projectdirs/atlas/${USER}/batch
```

# Running
The execution scripts are designed to run from within the `build/` directory.

## Making Ntuples
```bash
../HbbISR/scripts/runntupler.py direct
```

## Making Plots
```bash
../HbbISR/scripts/runall.sh direct
```

## Making Truth Ntuples
Typically one runs over private production of TRUTH derivations (ie: when validating new sample requests). The following instructions reflect that. However they are also valid for running over official TRUTH datasets by changing the input mode.

The list of files in a dataset should be specified in text file called `dataset1.txt`, with one file per line. There should also be a corresponding `dataset1.config` file containing the cross-section and generator filter efficiency in the following format (example values are used):
```
nEvents = 14000.000000
xsec = 9.115571e+02
filteff = 6.473857e-03
```

The truth ntuples can then be created using the `config_fatjet_truth.py` xAH configuration.
```
xAH_run.py --files dataset1.txt dataset2.txt --inputList --config ../HbbISR/data/config_fatjet_ntuple_truth.py --submitDir OUT_fatjet_truth -f direct
```

The output ntuples will be located in `OUT_fatjet_truth/data-tree/`.