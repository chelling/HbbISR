#include <EventLoop/Job.h>
#include <EventLoop/Worker.h>
#include <EventLoop/OutputStream.h>

#include <AsgTools/MessageCheck.h>

#include <HbbISR/HbbISRFatJetHistsAlgo.h>
#include <xAODAnaHelpers/HelperFunctions.h>

#include <TrigDecisionInterface/Conditions.h>

#include <utility>      
#include <iostream>
#include <fstream>

using namespace std;

// this is needed to distribute the algorithm to the workers
ClassImp(HbbISRFatJetHistsAlgo)

HbbISRFatJetHistsAlgo :: HbbISRFatJetHistsAlgo () :
  m_mc(false),
  m_jetDetailStr(""),
  m_fatjetDetailStr(""),
  m_doPUReweight(false),
  m_doCleaning(false),
  m_jetPtCleaningCut(25.),
  m_doTrigger(false),
  m_trigger(""),
  m_refTrigger(""),
  m_trigEffDen(false),
  m_jet0PtCut(0.),
  m_fatjet0PtCut(0.),
  m_fatjet0MCut(0.),
  hIncl(nullptr)
{
  Info("HbbISRFatJetHistsAlgo()", "Calling constructor");
}

EL::StatusCode HbbISRFatJetHistsAlgo :: histInitialize ()
{
  Info("histInitialize()", "Calling histInitialize \n");

  //
  // data model
  m_event=DijetISREvent::global();

  //
  // Cutflow
  m_cutflow=new CutflowHists(m_name, "");
  ANA_CHECK(m_cutflow->initialize());

  m_cf_trigger  =m_cutflow->addCut("trigger");
  m_cf_cleaning =m_cutflow->addCut("cleaning");
  if(m_jet0PtCut>0)    m_cf_jet0      =m_cutflow->addCut("jet0");
  if(m_fatjet0PtCut>0) m_cf_fatjet0pt =m_cutflow->addCut("fatjet0pt");
  if(m_fatjet0MCut>0)  m_cf_fatjet0m  =m_cutflow->addCut("fatjet0m");

  m_cutflow->record(wk());

  //
  // Histograms
  hIncl     =new HbbISRHists(m_name, "", m_jetDetailStr, "", "", m_fatjetDetailStr, "", "");
  ANA_CHECK(hIncl->initialize());
  hIncl->record(wk());

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode HbbISRFatJetHistsAlgo :: initialize ()
{
  if(m_debug) Info("initialize()", "Calling initialize");

  // Trigger
  std::string token;
  std::istringstream ss(m_trigger);

  while(std::getline(ss, token, ','))
    m_triggers.push_back(token);

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode HbbISRFatJetHistsAlgo :: execute ()
{
  ANA_MSG_DEBUG("HbbISRFatJetHistsAlgo::execute()");

  //
  // Cuts
  
  m_eventWeight    = m_event->m_weight;
  if(m_mc && m_doPUReweight)
    m_eventWeight *= m_event->m_weight_pileup;

  //
  // do trigger
  if(m_doTrigger)
    {
      ANA_MSG_DEBUG("Passed triggers:");
      for(const auto& trigger : *m_event->m_passedTriggers)
	ANA_MSG_DEBUG("\t" << trigger);

      ANA_MSG_DEBUG("Disabled triggers:");
      for(const auto& trigger : *m_event->m_disabledTriggers)
	ANA_MSG_DEBUG("\t" << trigger);

      //
      // trigger
      bool passTrigger=m_triggers.empty();
      bool presTrigger=true;
      bool disaTrigger=false;
      for(const std::string& trigger : m_triggers)
	{
	  passTrigger |= (std::find(m_event->m_passedTriggers->begin(), m_event->m_passedTriggers->end(), trigger ) != m_event->m_passedTriggers->end());
	  disaTrigger |= (std::find(m_event->m_disabledTriggers->begin(), m_event->m_disabledTriggers->end(), trigger ) != m_event->m_disabledTriggers->end());
	  presTrigger &= (((m_event->isPassBits(trigger)&TrigDefs::EF_prescaled)==TrigDefs::EF_prescaled)); // (m_event->prescale(trigger)<1)); need to check if trigger enabled
	}


      if(!m_refTrigger.empty()) 
	{ // Making trigger efficiency curve
	  bool passRefTrigger = (std::find(m_event->m_passedTriggers->begin(), m_event->m_passedTriggers->end(), m_refTrigger ) != m_event->m_passedTriggers->end());

	  if(m_trigEffDen)
	    passTrigger = (               passRefTrigger && !presTrigger && !disaTrigger);
	  else
	    passTrigger = (passTrigger && passRefTrigger && !presTrigger && !disaTrigger);
	}

      if(!passTrigger)
	{
	  ANA_MSG_DEBUG(" Fail Trigger");
	  return EL::StatusCode::SUCCESS;
	}
      m_cutflow->execute(m_cf_trigger, m_eventWeight);
    }

  //
  // doing cleaning
  //
  bool  passCleaning   = true;
  for(unsigned int i = 0;  i<m_event->jets(); ++i)
    {
      const xAH::Jet* jet=m_event->jet(i);
      if(jet->p4.Pt() > m_jetPtCleaningCut)
	{
	  if(!m_doCleaning && !jet->clean_passLooseBad)
	    {
	      ANA_MSG_DEBUG(" Skipping jet clean");
	      continue;
	    }
	  if(!jet->clean_passLooseBad) passCleaning = false;
	}
      else
	break;
    }

  //
  //  Jet Cleaning 
  //
  if(m_doCleaning && !passCleaning)
    {
      ANA_MSG_DEBUG(" Fail cleaning");
      return EL::StatusCode::SUCCESS;
    }
  m_cutflow->execute(m_cf_cleaning, m_eventWeight);

  //
  // Kinematic cuts
  if(m_jet0PtCut>0)
    {
      if(m_event->jets()<1)
	{
	  ANA_MSG_DEBUG(" Fail jetPt0 with 0 jets");
	  return EL::StatusCode::SUCCESS;
	}

      const xAH::Jet* jet0=m_event->jet(0);
      if(jet0->p4.Pt() < m_jet0PtCut)
	{
	  ANA_MSG_DEBUG(" Fail jetPt0 with " << jet0->p4.Pt());
	  return EL::StatusCode::SUCCESS;
	}
      m_cutflow->execute(m_cf_jet0,m_eventWeight);
    }

  const xAH::FatJet *fatjet0=nullptr;
  if(m_fatjet0PtCut>0 || m_fatjet0MCut>0)
    {
      if(m_event->fatjets()<1)
	{
	  ANA_MSG_DEBUG(" Fail fatjetPt0 with 0 fat jets");
	  return EL::StatusCode::SUCCESS;
	}
      fatjet0=m_event->fatjet(0);
    }

  if(m_fatjet0PtCut>0)
    {
      if(fatjet0->p4.Pt() < m_fatjet0PtCut)
	{
	  ANA_MSG_DEBUG(" Fail fatjetPt0 with " << fatjet0->p4.Pt());
	  return EL::StatusCode::SUCCESS;
	}
      m_cutflow->execute(m_cf_fatjet0pt,m_eventWeight);
    }


  if(m_fatjet0MCut>0)
    {
      if(fatjet0->p4.M() < m_fatjet0MCut)
	{
	  ANA_MSG_DEBUG(" Fail fatjetM0 with " << fatjet0->p4.M());
	  return EL::StatusCode::SUCCESS;
	}
      m_cutflow->execute(m_cf_fatjet0m,m_eventWeight);
    }

  ANA_MSG_DEBUG(" Pass All Cut");

  ANA_CHECK(histFill(m_eventWeight));

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode HbbISRFatJetHistsAlgo::histFill(float eventWeight)
{
  //
  //Filling
  hIncl->execute(*m_event, nullptr, nullptr, eventWeight);

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode HbbISRFatJetHistsAlgo :: histFinalize ()
{
  ANA_CHECK(m_cutflow->finalize());
  delete m_cutflow;

  ANA_CHECK(hIncl->finalize());
  delete hIncl;

  return EL::StatusCode::SUCCESS;
}

