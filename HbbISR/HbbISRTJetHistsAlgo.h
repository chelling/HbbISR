#ifndef HbbISR_HbbISRTJetHistsAlgo_H
#define HbbISR_HbbISRTJetHistsAlgo_H

// algorithm wrapper
#include <HbbISR/HbbISRHistsBaseAlgo.h>

class HbbISRTJetHistsAlgo : public HbbISRHistsBaseAlgo
{
public:
  bool m_skipDoubleTrkJet     = false; // Skip events with the Z' candidate having two trk jets
  bool m_allowSingleTrkJet    = false; // Allow jets with a single track jet, with a btag counting as 2
  bool m_skipBoostCut         = false; // Skip boost requirement
  bool m_skipVRContainCut     = false; // Skip the VR containment cut
  bool m_sortD2               = false; // Sort large R jets by D2 instead of pT
  uint m_HcandIdx             = 0; // Index of H candidate in the largeR jet list
  float m_trkjet1PtCut; // Minimum pT of subleading track jet
  xAH::Jet::BTaggerOP m_bTagWP; // Btagging working point.
  bool m_looseNoTight; // Count nBtags using jets failing a tight WP (m_BTagWP), but passing loose WP (85%). Vetoes events with tight btag.
  uint m_nBTagsCut; // Minimum number of b-tagged track jets
  bool m_nBTagsCutExact; // The nBTagsCut requires the exact number of btags (as opposed to same or more)

protected:
  virtual void initHcandCutflow();
  virtual void initBtagCutflow();
  virtual bool doHcandCutflow();
  virtual bool doBtagCutflow();

public:
  // this is a standard constructor
  HbbISRTJetHistsAlgo ();

private:
  bool btag(const xAH::Jet* trkjet) const;

  //
  // cutflow
  int m_cf_nfatjets;
  int m_cf_noDoubleTrkJet;
  int m_cf_hcand;
  int m_cf_trkjet1pt;
  int m_cf_notight;
  int m_cf_nbtag;

  // this is needed to distribute the algorithm to the workers
  ClassDef(HbbISRTJetHistsAlgo, 1);
};

#endif // HbbISR_HbbISRTJetHistsAlgo_H
