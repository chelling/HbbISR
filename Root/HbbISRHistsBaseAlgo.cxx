#include <EventLoop/Job.h>
#include <EventLoop/Worker.h>
#include <EventLoop/OutputStream.h>

#include <AsgTools/MessageCheck.h>

#include <HbbISR/HbbISRHistsBaseAlgo.h>

#include <xAODAnaHelpers/HelperFunctions.h>

#include <TrigDecisionInterface/Conditions.h>

#include <utility>      
#include <iostream>
#include <fstream>

using namespace std;

// this is needed to distribute the algorithm to the workers
ClassImp(HbbISRHistsBaseAlgo)

HbbISRHistsBaseAlgo :: HbbISRHistsBaseAlgo () :
  m_mc(false),
  m_histDetailStr(""),
  m_jetDetailStr(""),
  m_fatjetDetailStr(""),
  m_subjetDetailStr(""),
  m_muonDetailStr(""),
  m_electronDetailStr(""),
  m_doPUReweight(false),
  m_trigger(""),
  m_refTrigger(""),
  m_trigEffDen(false),
  m_jet0PtCut(0.),
  m_fatjet0PtCut(0.),
  m_trkJet("GhostAntiKt2TrackJet"),
  m_doBoostCut(false),
  m_details(nullptr),
  hIncl(nullptr),
  hM0to50   (nullptr),
  hM50to100 (nullptr),
  hM75to125 (nullptr),
  hM100to150(nullptr),
  hM125to175(nullptr),
  hM150to200(nullptr),
  hM175to225(nullptr),
  hM200to250(nullptr),
  hM225to275(nullptr),
  hM250to300(nullptr)
{
  Info("HbbISRHistsBaseAlgo()", "Calling constructor");
}

EL::StatusCode HbbISRHistsBaseAlgo :: setupJob (EL::Job& job)
{
  ANA_MSG_INFO("HbbISRHistsBaseAlgo::setupJob()");

  size_t slashidx=m_name.find_last_of('/');
  if(slashidx==std::string::npos)
    {
      m_treeDirectory=m_name;
      m_treeName="outTree";
    }
  else
    {
      m_treeDirectory=m_name.substr(0,slashidx);
      m_treeName="outTree"+m_name.substr(slashidx+4);
    }

  if(!job.outputHas(m_treeDirectory))
    {
      EL::OutputStream outForTree( m_treeDirectory );
      job.outputAdd (outForTree);
    }
  
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode HbbISRHistsBaseAlgo :: histInitialize ()
{
  ANA_MSG_INFO("HbbISRHistsBaseAlgo::histInitialize()");

  //
  // Cutflow
  m_cutflow=new CutflowHists(m_name, "");
  ANA_CHECK(m_cutflow->initialize());

  m_cf_trigger  =m_cutflow->addCut("trigger");
  m_cf_cleaning =m_cutflow->addCut("cleaning");
  if(m_jet0PtCut>0)    m_cf_jet0      =m_cutflow->addCut("jet0");
  if(m_fatjet0PtCut>0) m_cf_fatjet0   =m_cutflow->addCut("fatjet0");
  if(m_fatjet1PtCut>0) m_cf_fatjet1   =m_cutflow->addCut("fatjet1");
  initHcandCutflow();

  if(m_doTTbarMuonCR)
    { // ttbarCR cutflow objects
      m_cf_TTbar_muonPhi           =m_cutflow->addCut("TTbarMuonPhi"           );
      m_cf_TTbar_muonPt            =m_cutflow->addCut("TTbarMuonPt"            );
      m_cf_TTbar_muonFatJetPt      =m_cutflow->addCut("TTbarMuonFatJetPt"      );
      m_cf_TTbar_muonDeltaR        =m_cutflow->addCut("TTbarMuonDeltaR"        );
      m_cf_TTbar_FatJetHiggsPhi    =m_cutflow->addCut("TTbarFatJetHiggsPhi"    );
      m_cf_TTbar_SufficientNFatJets=m_cutflow->addCut("TTbarSufficientNFatJets");
      m_cf_TTbar_SufficientNbJets  =m_cutflow->addCut("TTbarSufficientNbJets"  );
    }

  if(m_vetoTTbarMuonCR)
    { // veto ttbarCR cutflow objects
      m_cf_TTbar_muon              =m_cutflow->addCut("NoTTbarMuon"            );
    }

  if(m_HcandIdxCut>=0) m_cf_hcandidx  =m_cutflow->addCut("hcandidx");
  if(m_HcandPtCut>0)   m_cf_hcandpt   =m_cutflow->addCut("hcandpt");
  if(m_HcandMCut>0)    m_cf_hcandm    =m_cutflow->addCut("hcandm");
  if(m_doBoostCut)     m_cf_boost     =m_cutflow->addCut("boost");

  initBtagCutflow();

  m_cutflow->record(wk());

  //
  // Histograms
  m_details=new HbbISRHelperClasses::HbbISRInfoSwitch(m_histDetailStr);

  hIncl    =new HbbISRHists(m_name, m_histDetailStr, m_jetDetailStr, m_muonDetailStr, m_electronDetailStr, m_fatjetDetailStr, m_trkJet, m_subjetDetailStr);
  ANA_CHECK(hIncl->initialize());
  hIncl->record(wk());

  if(m_details->m_mass)
    {
      hM0to50   =new HbbISRHists(m_name+"/M50to100/" , "", "kinematic", "kinematic", "", m_fatjetDetailStr, m_trkJet, m_subjetDetailStr);
      ANA_CHECK(hM0to50   ->initialize());
      hM0to50  ->record(wk());

      hM50to100 =new HbbISRHists(m_name+"/M50to100/" , "", "kinematic", "kinematic", "", m_fatjetDetailStr, m_trkJet, m_subjetDetailStr);
      ANA_CHECK(hM50to100 ->initialize());
      hM50to100->record(wk());

      hM75to125 =new HbbISRHists(m_name+"/M75to125/" , "", "kinematic", "kinematic", "", m_fatjetDetailStr, m_trkJet, m_subjetDetailStr);
      ANA_CHECK(hM75to125 ->initialize());
      hM75to125->record(wk());

      hM100to150=new HbbISRHists(m_name+"/M100to150/", "", "kinematic", "kinematic", "", m_fatjetDetailStr, m_trkJet, m_subjetDetailStr);
      ANA_CHECK(hM100to150->initialize());
      hM100to150->record(wk());

      hM125to175=new HbbISRHists(m_name+"/M125to175/", "", "kinematic", "kinematic", "", m_fatjetDetailStr, m_trkJet, m_subjetDetailStr);
      ANA_CHECK(hM125to175->initialize());
      hM125to175->record(wk());

      hM150to200=new HbbISRHists(m_name+"/M150to200/", "", "kinematic", "kinematic", "", m_fatjetDetailStr, m_trkJet, m_subjetDetailStr);
      ANA_CHECK(hM150to200->initialize());
      hM150to200->record(wk());

      hM175to225=new HbbISRHists(m_name+"/M175to225/" , "", "kinematic", "kinematic", "", m_fatjetDetailStr, m_trkJet, m_subjetDetailStr);
      ANA_CHECK(hM175to225 ->initialize());
      hM175to225->record(wk());

      hM200to250=new HbbISRHists(m_name+"/M200to250/", "", "kinematic", "kinematic", "", m_fatjetDetailStr, m_trkJet, m_subjetDetailStr);
      ANA_CHECK(hM200to250->initialize());
      hM200to250->record(wk());

      hM225to275=new HbbISRHists(m_name+"/M225to275/", "", "kinematic", "kinematic", "", m_fatjetDetailStr, m_trkJet, m_subjetDetailStr);
      ANA_CHECK(hM225to275->initialize());
      hM225to275->record(wk());

      hM250to300=new HbbISRHists(m_name+"/M250to300/", "", "kinematic", "kinematic", "", m_fatjetDetailStr, m_trkJet, m_subjetDetailStr);
      ANA_CHECK(hM250to300->initialize());
      hM250to300->record(wk());
    }

  if(m_details->m_pt)
    {
      std::stringstream ss;
      for(uint i=0;i<10;i++)
	{
	  ss.str("");
	  ss << m_name << "/Pt" << 250+i*100 << "to" << 250+(i+1)*100 << "/";
	  hPt[i] =new HbbISRHists(ss.str() , "", "kinematic", "kinematic", "", m_fatjetDetailStr, m_trkJet, m_subjetDetailStr);
	  ANA_CHECK(hPt[i]->initialize());
	  hPt[i]->record(wk());
	}
    }

  if(m_mc && m_details->m_truthtag)
    {
      hBB=new HbbISRHists(m_name+"/BB/", "", "kinematic", "kinematic", "", m_fatjetDetailStr, m_trkJet, m_subjetDetailStr);
      ANA_CHECK(hBB->initialize());
      hBB->record(wk());

      hCC=new HbbISRHists(m_name+"/CC/", "", "kinematic", "kinematic", "", m_fatjetDetailStr, m_trkJet, m_subjetDetailStr);
      ANA_CHECK(hCC->initialize());
      hCC->record(wk());

      hLL=new HbbISRHists(m_name+"/LL/", "", "kinematic", "kinematic", "", m_fatjetDetailStr, m_trkJet, m_subjetDetailStr);
      ANA_CHECK(hLL->initialize());
      hLL->record(wk());

      hBC=new HbbISRHists(m_name+"/BC/", "", "kinematic", "kinematic", "", m_fatjetDetailStr, m_trkJet, m_subjetDetailStr);
      ANA_CHECK(hBC->initialize());
      hBC->record(wk());

      hBL=new HbbISRHists(m_name+"/BL/", "", "kinematic", "kinematic", "", m_fatjetDetailStr, m_trkJet, m_subjetDetailStr);
      ANA_CHECK(hBL->initialize());
      hBL->record(wk());

      hCL=new HbbISRHists(m_name+"/CL/", "", "kinematic", "kinematic", "", m_fatjetDetailStr, m_trkJet, m_subjetDetailStr);
      ANA_CHECK(hCL->initialize());
      hCL->record(wk());

      hB =new HbbISRHists(m_name+"/B/" , "", "kinematic", "kinematic", "", m_fatjetDetailStr, m_trkJet, m_subjetDetailStr);
      ANA_CHECK(hB ->initialize());
      hB ->record(wk());

      hC =new HbbISRHists(m_name+"/C/" , "", "kinematic", "kinematic", "", m_fatjetDetailStr, m_trkJet, m_subjetDetailStr);
      ANA_CHECK(hC ->initialize());
      hC ->record(wk());

      hL =new HbbISRHists(m_name+"/L/" , "", "kinematic", "kinematic", "", m_fatjetDetailStr, m_trkJet, m_subjetDetailStr);
      ANA_CHECK(hL ->initialize());
      hL ->record(wk());

      hXX=new HbbISRHists(m_name+"/XX/", "", "kinematic", "kinematic", "", m_fatjetDetailStr, m_trkJet, m_subjetDetailStr);
      ANA_CHECK(hXX->initialize());
      hXX->record(wk());
    }

  //
  // TTree
  TFile* treeFile = wk()->getOutputFile(m_treeDirectory);
  m_tree = new TTree(m_treeName.c_str(), m_treeName.c_str());
  m_tree->SetDirectory(treeFile);
  m_tree->SetAutoFlush(-500000);

  m_tree->Branch("weight", &br_eventWeight, "weight/F");
  m_tree->Branch("mcChannelNumber", &br_mcChannelNumber, "mcChannelNumber/I");
  m_tree->Branch("Hcand_p4", &br_Hcand_p4);

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode HbbISRHistsBaseAlgo :: changeInput (bool firstFile)
{
  ANA_MSG_INFO("HbbISRHistsBaseAlgo::changeInput(" << firstFile << ")");

  //
  // Update cutflow hists
  TFile* inputFile = wk()->inputFile();

  TH1D* MetaData_EventCount=dynamic_cast<TH1D*>(inputFile->Get("MetaData_EventCount_Test"));
  if(!MetaData_EventCount)
    {
      ANA_MSG_ERROR("Missing input event count histogram!");
      return EL::StatusCode::FAILURE;
    }

  float totalEvents= MetaData_EventCount->GetBinContent(1);
  float totalWeight= MetaData_EventCount->GetBinContent(3);

  m_cutflow->executeInitial(totalEvents, totalWeight);

  return EL::StatusCode::SUCCESS;
}


EL::StatusCode HbbISRHistsBaseAlgo :: initialize ()
{
  ANA_MSG_INFO("HbbISRHistsBaseAlgo::initialize()");

  // Trigger
  std::string token;
  std::istringstream ss(m_trigger);

  while(std::getline(ss, token, ','))
    m_triggers.push_back(token);

  //
  // data model
  if(m_JMRuncert==JMRUncertaintyAlgo::NOMINAL) m_event=DijetISREvent::global();
  else                                         m_event=JMRUncertaintyAlgo::global(m_JMRuncert);

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode HbbISRHistsBaseAlgo :: execute ()
{
  ANA_MSG_DEBUG("HbbISRHistsBaseAlgo::execute()");

  //
  // Cuts

  m_eventWeight    = m_event->m_weight;
  if(m_mc && m_doPUReweight)
    m_eventWeight *= m_event->m_weight_pileup;

  //
  // do trigger
  if(m_doTrigger)
    {
      ANA_MSG_DEBUG("Passed triggers:");
      for(const auto& trigger : *m_event->m_passedTriggers)
	ANA_MSG_DEBUG("\t" << trigger);

      ANA_MSG_DEBUG("Disabled triggers:");
      for(const auto& trigger : *m_event->m_disabledTriggers)
	ANA_MSG_DEBUG("\t" << trigger);

      //
      // trigger
      bool passTrigger=m_triggers.empty();
      bool presTrigger=true;
      bool disaTrigger=false;
      for(const std::string& trigger : m_triggers)
	{
	  passTrigger |= (std::find(m_event->m_passedTriggers  ->begin(), m_event->m_passedTriggers  ->end(), trigger ) != m_event->m_passedTriggers  ->end());
	  disaTrigger |= (std::find(m_event->m_disabledTriggers->begin(), m_event->m_disabledTriggers->end(), trigger ) != m_event->m_disabledTriggers->end());
	  presTrigger &= (((m_event->isPassBits(trigger)&TrigDefs::EF_prescaled)==TrigDefs::EF_prescaled) );
	}


      if(!m_refTrigger.empty()) 
	{ // Making trigger efficiency curve
	  bool passRefTrigger = (std::find(m_event->m_passedTriggers->begin(), m_event->m_passedTriggers->end(), m_refTrigger ) != m_event->m_passedTriggers->end());

	  if(m_trigEffDen)
	    passTrigger = (               passRefTrigger && !presTrigger && !disaTrigger);
	  else
	    passTrigger = (passTrigger && passRefTrigger && !presTrigger && !disaTrigger);
	}

      if(!passTrigger)
	{
	  ANA_MSG_DEBUG(" Fail Trigger");
	  return EL::StatusCode::SUCCESS;
	}
      m_cutflow->execute(m_cf_trigger, m_eventWeight);
    }

  //
  // doing cleaning
  //

  //
  //  Jet Cleaning 
  //
  if(m_doCleaning)
    {
      bool passCleaning = true;
      for(unsigned int i = 0; i<m_event->jets(); ++i)
	{
	  const xAH::Jet* jet=m_event->jet(i);
	  if(jet->p4.Pt() > m_jetPtCleaningCut)
	    {
	      if(!jet->clean_passLooseBad) 
		{
		  passCleaning = false;
		  break;
		}
	    }
	}

      if(!passCleaning)
	{
	  ANA_MSG_DEBUG(" Fail cleaning");
	  return EL::StatusCode::SUCCESS;
	}
      m_cutflow->execute(m_cf_cleaning, m_eventWeight);
    }

  //
  // Kinematic cuts
  if(m_jet0PtCut>0)
    {
      if(m_event->jets()<1)
	{
	  ANA_MSG_DEBUG(" Fail jetPt0 with 0 jets");
	  return EL::StatusCode::SUCCESS;
	}

      const xAH::Jet* jet0=m_event->jet(0);
      if(jet0->p4.Pt() < m_jet0PtCut)
	{
	  ANA_MSG_DEBUG(" Fail jetPt0 with " << jet0->p4.Pt());
	  return EL::StatusCode::SUCCESS;
	}
      m_cutflow->execute(m_cf_jet0,m_eventWeight);
    }

  if(m_fatjet0PtCut>0)
    {
      if(m_event->fatjets()<1)
	{
	  ANA_MSG_DEBUG(" Fail fatjetPt0 with <1 fat jets");
	  return EL::StatusCode::SUCCESS;
	}

      const xAH::FatJet* fatjet0=m_event->fatjet(0);
      if(fatjet0->p4.Pt() < m_fatjet0PtCut)
	{
	  ANA_MSG_DEBUG(" Fail fatjetPt0 with " << fatjet0->p4.Pt());
	  return EL::StatusCode::SUCCESS;
	}
      m_cutflow->execute(m_cf_fatjet0,m_eventWeight);
    }

  if(m_fatjet1PtCut>0)
    {
      if(m_event->fatjets()<2)
	{
	  ANA_MSG_DEBUG(" Fail fatjetPt1 with <2 fat jets");
	  return EL::StatusCode::SUCCESS;
	}

      const xAH::FatJet* fatjet1=m_event->fatjet(1);
      if(fatjet1->p4.Pt() < m_fatjet1PtCut)
	{
	  ANA_MSG_DEBUG(" Fail fatjetPt1 with " << fatjet1->p4.Pt());
	  return EL::StatusCode::SUCCESS;
	}
      m_cutflow->execute(m_cf_fatjet1,m_eventWeight);
    }

  //
  // Higgs candidate part of the selection
  m_Hcand   =nullptr;
  m_nonHcand=nullptr;
  if(!doHcandCutflow())
    return EL::StatusCode::SUCCESS;

  // Muon Requirement for ttbar control region
  if(m_doTTbarMuonCR)
    {
      // Requirements on Muon for ttbar event
      if(m_event->muons()<1)
	  return EL::StatusCode::SUCCESS;

      bool pass_ttbar_muonPhi_flag = false;
      bool pass_ttbar_muonPt_flag = false;
      ttbar_muon = nullptr;
      // This for loop searches for a muon in the event which satisfies our ttbar CR requirements
      for(unsigned int j = 0; j<m_event->muons(); ++j)
	{
	  const xAH::Muon* muon=m_event->muon(j);
	  if( muon->p4.Pt() > m_muonpT )
	    { // Require high muon pT to reject soft b decays from gluon pair production
	      pass_ttbar_muonPt_flag = true;	      
	      if( fabs(m_Hcand->p4.DeltaPhi(muon->p4)) > ((2.0 * M_PI) / 3.0) )
		{
		  // Require muon be in opposite hemisphere of Hcand
		  pass_ttbar_muonPhi_flag = true;
		  ttbar_muon = m_event->muon(j);
		  break;
		}
	    }
	  else
	    break; // Muons are pT sorted, won't find anything passing the pT cut after this
	}

      // Keep event if muon found
      if(!pass_ttbar_muonPhi_flag) return EL::StatusCode::SUCCESS;
      m_cutflow->execute(m_cf_TTbar_muonPhi,m_eventWeight);
      if(!pass_ttbar_muonPt_flag) return EL::StatusCode::SUCCESS;
      m_cutflow->execute(m_cf_TTbar_muonPt,m_eventWeight);

      // Requirements on fatjet associated with muon for ttbar event
      bool pass_ttbar_muonFatJetPt_flag = false;
      bool pass_ttbar_muonDeltaR_flag = false;
      bool pass_ttbar_FatJetHiggsPhi_flag = false;
      std::vector<const xAH::FatJet *> fatjets_near_muon; // Vector to collect all fatjets which can be associated with the choosen muon

      for(unsigned int fatjet_itr = 0; fatjet_itr < m_event->fatjets(); ++fatjet_itr)
	{
	  // Loop through fatjets to find one close to the muon, on the opposite hemisphere of the Hcand
	  const xAH::FatJet* fatjet_opp = m_event->fatjet(fatjet_itr);
	  if(fatjet_opp->p4.Pt() > 250)
	    {
	      // Skip any fatjets that are less than 250GeV, since the as they are probably QCD 
	      pass_ttbar_muonFatJetPt_flag = true;
	      if ( fatjet_opp->p4.DeltaR(ttbar_muon->p4) > 0.04+10.0/ttbar_muon->p4.Pt() && fatjet_opp->p4.DeltaR(ttbar_muon->p4) < 1.5)
		{ // First requirement is similar to muon isolation requirement.  2nd one improves agreement between data and simulation around W/Z peak
		  // Skip any fatjet which is greater than 1.5 dR away from muon or isnt properly isolated
		  pass_ttbar_muonDeltaR_flag = true;
		  if ( fabs( m_Hcand->p4.DeltaPhi(fatjet_opp->p4) ) > ((2.0 * M_PI) / 3.0) )
		    {
		      // Skip fatjets that aren't in the opposite hemisphere of Hcand
		      pass_ttbar_FatJetHiggsPhi_flag = true;
		      fatjets_near_muon.push_back(fatjet_opp);
		    }
		}
	    }
	}
      if(!pass_ttbar_muonFatJetPt_flag) return EL::StatusCode::SUCCESS; // Skip any event where there are no fatjet_opp with pt > 250GeV
      m_cutflow->execute(m_cf_TTbar_muonFatJetPt,m_eventWeight);
      if(!pass_ttbar_muonDeltaR_flag) return EL::StatusCode::SUCCESS; // Skip any event where the fatjet_opp and Muon are too far away from each other
      m_cutflow->execute(m_cf_TTbar_muonDeltaR,m_eventWeight);
      if(!pass_ttbar_FatJetHiggsPhi_flag) return EL::StatusCode::SUCCESS; // Skip any event where the fatjet_opp and higgs_cand are too close to eachother
      m_cutflow->execute(m_cf_TTbar_FatJetHiggsPhi,m_eventWeight);
      if(fatjets_near_muon.size() < 1) return EL::StatusCode::SUCCESS; // Skip any event where we don't have a FatJet near the muon
      m_cutflow->execute(m_cf_TTbar_SufficientNFatJets,m_eventWeight);

      std::vector<xAH::Jet> trkJets_near_muon=fatjets_near_muon.at(0)->trkJets.at(m_trkJet); // Only look at leading fatjet we associated with the muon
      int nbJets = 0; // Count number of b tagged track jets found in fatjet opposite the Hcand
      if(trkJets_near_muon.size()>1)
	{
	  const xAH::Jet& trkJet=trkJets_near_muon.at(0); // Only look at b-tagging the leading track jet
	  if(btag_muon(&trkJet)) nbJets++; // Btag for the leading jet
	}
      if(nbJets < m_nBjet_opp) return EL::StatusCode::SUCCESS; // Skip any event where we have less bjet_opp in fatjet_opp than requested
      m_cutflow->execute(m_cf_TTbar_SufficientNbJets,m_eventWeight);

      m_nonHcand = fatjets_near_muon.at(0); // The fatjet associated with the muon is our nonHcand fatjet 
    } // End of m_doTTbarMuonCR

  // Muon Requirement for ttbar control region veto
  if(m_vetoTTbarMuonCR)
    {
      bool pass_ttbar_muonPhi_flag = false;
      bool pass_ttbar_muonPt_flag = false;

      // Requirements on Muon for ttbar event
      if(m_event->muons()>0)
	{
	  ttbar_muon = nullptr;
	  // This for loop searches for a muon in the event which satisfies our ttbar CR requirements
	  for(unsigned int j = 0; j<m_event->muons(); ++j)
	    {
	      const xAH::Muon* muon=m_event->muon(j);
	      if( muon->p4.Pt() > m_muonpT )
		{ // Require high muon pT to reject soft b decays from gluon pair production
		  pass_ttbar_muonPt_flag = true;	      
		  if( fabs(m_Hcand->p4.DeltaPhi(muon->p4)) > ((2.0 * M_PI) / 3.0) )
		    {
		      // Require muon be in opposite hemisphere of Hcand
		      pass_ttbar_muonPhi_flag = true;
		      ttbar_muon = m_event->muon(j);
		      break;
		    }
		}
	      else
		break; // Muons are pT sorted, won't find anything passing the pT cut after this
	    }
	}

      // Veto events if muon found
      if(pass_ttbar_muonPhi_flag && pass_ttbar_muonPt_flag) return EL::StatusCode::SUCCESS;
      m_cutflow->execute(m_cf_TTbar_muon,m_eventWeight);
    }

  //
  // Extra common cuts
  if(m_HcandIdxCut>=0)
    {
      if(m_Hcand!=m_event->fatjet(m_HcandIdxCut))
	{
	  ANA_MSG_DEBUG(" Fail Hcand idx");
	  return EL::StatusCode::SUCCESS;	  
	}
      m_cutflow->execute(m_cf_hcandidx,m_eventWeight);
    }

  if(m_HcandPtCut>0)
    {
      if(m_Hcand->p4.Pt()<m_HcandPtCut)
	{
	  ANA_MSG_DEBUG(" Fail Hcand pt with " << m_Hcand->p4.Pt());
	  return EL::StatusCode::SUCCESS;	  
	}
      m_cutflow->execute(m_cf_hcandpt,m_eventWeight);
    }

  if(m_HcandMCut>0)
    {
      if(m_Hcand->p4.M()<m_HcandMCut)
	{
	  ANA_MSG_DEBUG(" Fail Hcand m with " << m_Hcand->p4.M());
	  return EL::StatusCode::SUCCESS;	  
	}
      m_cutflow->execute(m_cf_hcandm,m_eventWeight);
    }

  if(m_doBoostCut)
    {
      float boostR=2*m_Hcand->p4.M()/m_Hcand->p4.Pt();
      if(boostR>1)
	{
	  ANA_MSG_DEBUG(" Fail boost check with " << boostR);
	  return EL::StatusCode::SUCCESS;	  
	}
      m_cutflow->execute(m_cf_boost,m_eventWeight);
    }

  //
  // Apply b-tagging
  if(!doBtagCutflow())
    return EL::StatusCode::SUCCESS;
  
  ANA_MSG_DEBUG(" Pass All Cut");

  ANA_CHECK(histFill(m_eventWeight));
  ANA_CHECK(treeFill(m_eventWeight));

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode HbbISRHistsBaseAlgo::histFill(float eventWeight)
{
  //
  //Filling
  hIncl->execute(*m_event, m_Hcand, m_nonHcand, eventWeight);

  double m  =m_Hcand->p4.M();
  double pt =m_Hcand->p4.Pt();

  if(m_details->m_mass)
    {
      if(m >  0 && m <=  50)
	hM0to50   ->execute(*m_event, m_Hcand, m_nonHcand, eventWeight);
      else if(m >  50 && m <= 100)
	hM50to100 ->execute(*m_event, m_Hcand, m_nonHcand, eventWeight);
      else if(m > 100 && m <= 150)
	hM100to150->execute(*m_event, m_Hcand, m_nonHcand, eventWeight);
      else if(m > 150 && m <= 200)
	hM150to200->execute(*m_event, m_Hcand, m_nonHcand, eventWeight);
      else if(m > 200 && m <= 250)
	hM200to250->execute(*m_event, m_Hcand, m_nonHcand, eventWeight);
      else if(m > 250 && m <= 300)
	hM250to300->execute(*m_event, m_Hcand, m_nonHcand, eventWeight);

      if(m >  75 && m <= 125)
	hM75to125 ->execute(*m_event, m_Hcand, m_nonHcand, eventWeight);
      else if(m > 125 && m <= 175)
	hM125to175->execute(*m_event, m_Hcand, m_nonHcand, eventWeight);
      else if(m > 175 && m <= 225)
	hM175to225->execute(*m_event, m_Hcand, m_nonHcand, eventWeight);
      else if(m > 225 && m <= 275)
	hM225to275->execute(*m_event, m_Hcand, m_nonHcand, eventWeight);
    }

  if(m_details->m_pt)
    {
      for(uint i=0;i<10;i++)
	{
	  if(250+i*100<pt && pt<250+(i+1)*100)
	    {
	      hPt[i]->execute(*m_event, m_Hcand, m_nonHcand, eventWeight);
	      break;
	    }
	}
    }
  
  if(m_mc && m_details->m_truthtag && !m_trkJet.empty())
    {
      const std::vector<xAH::Jet> &trkJets=m_Hcand->trkJets.at(m_trkJet);

      if(trkJets.size()>=2)
	{
	  const xAH::Jet *tjet0=&trkJets.at(0);
	  const xAH::Jet *tjet1=&trkJets.at(1);

	  int tjet0id=tjet0->HadronConeExclTruthLabelID;
	  int tjet1id=tjet1->HadronConeExclTruthLabelID;

	  if(tjet0id==5 && tjet1id==5)
	    hBB->execute(*m_event, m_Hcand, m_nonHcand, eventWeight);
	  else if(tjet0id==4 && tjet1id==4)
	    hCC->execute(*m_event, m_Hcand, m_nonHcand, eventWeight);
	  else if(tjet0id==0 && tjet1id==0)
	    hLL->execute(*m_event, m_Hcand, m_nonHcand, eventWeight);
	  else if((tjet0id==5 && tjet1id==4) || (tjet0id==4 && tjet1id==5))
	    hBC->execute(*m_event, m_Hcand, m_nonHcand, eventWeight);
	  else if((tjet0id==5 && tjet1id==0) || (tjet0id==0 && tjet1id==5))
	    hBL->execute(*m_event, m_Hcand, m_nonHcand, eventWeight);
	  else if((tjet0id==4 && tjet1id==0) || (tjet0id==0 && tjet1id==4))
	    hCL->execute(*m_event, m_Hcand, m_nonHcand, eventWeight);
	  else
	    hXX->execute(*m_event, m_Hcand, m_nonHcand, eventWeight);
	}
      else if(trkJets.size()==1)
	{
	  const xAH::Jet *tjet0=&trkJets.at(0);

	  int tjet0id=tjet0->HadronConeExclExtendedTruthLabelID;

	  if(tjet0id==55)
	    hBB->execute(*m_event, m_Hcand, m_nonHcand, eventWeight);
	  else if(tjet0id==44)
	    hCC->execute(*m_event, m_Hcand, m_nonHcand, eventWeight);
	  else if(tjet0id==5)
	    hB ->execute(*m_event, m_Hcand, m_nonHcand, eventWeight);
	  else if(tjet0id==4)
	    hC ->execute(*m_event, m_Hcand, m_nonHcand, eventWeight);
	  else if(tjet0id==0)
	    hL ->execute(*m_event, m_Hcand, m_nonHcand, eventWeight);
	  else
	    hXX->execute(*m_event, m_Hcand, m_nonHcand, eventWeight);
	}
    }

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode HbbISRHistsBaseAlgo::treeFill(float eventWeight)
{
  // Event Info
  br_eventWeight=eventWeight;
  br_mcChannelNumber=m_event->m_mcChannelNumber;

  // Hcand
  br_Hcand_p4.SetPtEtaPhiM(m_Hcand->p4.Pt(), m_Hcand->p4.Eta(), m_Hcand->p4.Phi(), m_Hcand->p4.M());

  //  Save
  m_tree->Fill();

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode HbbISRHistsBaseAlgo :: histFinalize ()
{
  ANA_MSG_INFO("HbbISRHistsBaseAlgo::histFinalize()");

  //
  // Histograms
  ANA_CHECK(hIncl->finalize());
  delete hIncl;

  if(m_details->m_mass)
    {
      ANA_CHECK(hM0to50   ->finalize());
      delete hM0to50;

      ANA_CHECK(hM50to100 ->finalize());
      delete hM50to100;

      ANA_CHECK(hM75to125 ->finalize());
      delete hM75to125;

      ANA_CHECK(hM100to150->finalize());
      delete hM100to150;

      ANA_CHECK(hM125to175->finalize());
      delete hM125to175;

      ANA_CHECK(hM150to200->finalize());
      delete hM150to200;

      ANA_CHECK(hM175to225->finalize());
      delete hM175to225;

      ANA_CHECK(hM200to250->finalize());
      delete hM200to250;

      ANA_CHECK(hM225to275->finalize());
      delete hM225to275;

      ANA_CHECK(hM250to300->finalize());
      delete hM250to300;
    }

  if(m_details->m_pt)
    {
      for(uint i=0;i<10;i++)
	{
	  ANA_CHECK(hPt[i]);
	  delete hPt[i];
	}
    }

  if(m_mc && m_details->m_truthtag)
    {
      ANA_CHECK(hBB->finalize());
      delete hBB;

      ANA_CHECK(hCC->finalize());
      delete hCC;

      ANA_CHECK(hLL->finalize());
      delete hLL;

      ANA_CHECK(hBC->finalize());
      delete hBC;

      ANA_CHECK(hBL->finalize());
      delete hBL;

      ANA_CHECK(hCL->finalize());
      delete hCL;

      ANA_CHECK(hB ->finalize());
      delete hB ;

      ANA_CHECK(hC ->finalize());
      delete hC ;

      ANA_CHECK(hL ->finalize());
      delete hL ;

      ANA_CHECK(hXX->finalize());
      delete hXX;
    }

  //
  // Tree fill
  TFile* treeFile=wk()->getOutputFile( m_treeDirectory );
  m_cutflow->write(treeFile);

  //
  // Cutflow
  ANA_CHECK(m_cutflow->finalize());
  delete m_cutflow;

  return EL::StatusCode::SUCCESS;
}

bool HbbISRHistsBaseAlgo::btag_muon(const xAH::Jet* trkjet) const
// ttbarCR function that returns decision of whether or not the given trackjet passes the tight b-tagging requirement
{
  bool tight=trkjet->is_btag(xAH::Jet::MV2c10_FixedCutBEff_77);
  return tight;
}
