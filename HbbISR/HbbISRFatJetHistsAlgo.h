#ifndef HbbISR_HbbISRFatJetHistsAlgo_H
#define HbbISR_HbbISRFatJetHistsAlgo_H

#include <EventLoop/StatusCode.h>
#include <EventLoop/Algorithm.h>
#include <EventLoop/Worker.h>

//algorithm wrapper
#include <xAODAnaHelpers/Algorithm.h>

// our histogramming code
#include <ZprimeDM/DijetISREvent.h>
#include <ZprimeDM/CutflowHists.h>

#include <HbbISR/HbbISRHelperClasses.h>
#include <HbbISR/HbbISRHists.h>

#include <sstream>
#include <vector>

class HbbISRFatJetHistsAlgo : public xAH::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  //configuration variables
  bool m_mc;

  // switches
  std::string m_jetDetailStr;
  std::string m_fatjetDetailStr;

  bool m_doPUReweight;
  bool m_doCleaning;
  float m_jetPtCleaningCut;

  // trigger config
  bool m_doTrigger;  
  /** @brief Comma-separated list of triggers that must pass (OR) */
  std::string m_trigger;
  /** @brief Reference trigger to trigger turn on curves.

      Added to m_trigger as AND. Also requires that none of the triggers in m_trigger
      have been prescaled away.
   */
  std::string m_refTrigger;
  /** @brief Make denominator for trigger efficiency curve
   *
   * Ignores the decision of m_trigger, but still requires that none of them have been
   * prescaled away.
   */
  bool m_trigEffDen;

  // Kinematic selection
  float m_jet0PtCut;
  float m_fatjet0PtCut;
  float m_fatjet0MCut;

private:

  //
  // Cutflow
  int m_cf_trigger;
  int m_cf_cleaning;
  int m_cf_jet0;
  int m_cf_fatjet0pt;
  int m_cf_fatjet0m;

  int m_cf_blindttbar;

  //
  // Histograms
  HbbISRHelperClasses::HbbISRInfoSwitch *m_details; //!

  HbbISRHists *hIncl; //!

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)

protected:
  // Cutflow data
  CutflowHists *m_cutflow; //!

  // Event data
  DijetISREvent *m_event; //!

  //
  // Parsed configuration
  std::vector<std::string> m_triggers;

  // ISR selection
  float m_eventWeight;

public:
  // Tree *myTree; //!
  // TH1 *myHist; //!

  // this is a standard constructor
  HbbISRFatJetHistsAlgo ();

  // these are the functions inherited from Algorithm
  EL::StatusCode histInitialize ();
  EL::StatusCode initialize ();
  EL::StatusCode execute ();
  EL::StatusCode histFill (float eventWeight);
  EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(HbbISRFatJetHistsAlgo, 1);
};

#endif // HbbISR_HbbISRFatJetHistsAlgo_H
