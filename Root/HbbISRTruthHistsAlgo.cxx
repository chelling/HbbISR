#include <HbbISR/HbbISRTruthHistsAlgo.h>

#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>

#include <AsgTools/MessageCheck.h>

#include <xAODAnaHelpers/HelperFunctions.h>
#include <xAODAnaHelpers/HelperClasses.h>

#include <algorithm>

// this is needed to distribute the algorithm to the workers
ClassImp(HbbISRTruthHistsAlgo)

HbbISRTruthHistsAlgo :: HbbISRTruthHistsAlgo ()
: HbbISRHistsBaseAlgo()
{ }

void HbbISRTruthHistsAlgo :: initHcandCutflow ()
{
  m_cf_nfatjets = m_cutflow->addCut("NFatJets");
  m_cf_match    = m_cutflow->addCut("HMatch");
}

void HbbISRTruthHistsAlgo :: initBtagCutflow ()
{ }

bool HbbISRTruthHistsAlgo :: doHcandCutflow ()
{
  uint nfatjets = m_event->fatjets();

  if(nfatjets < 1)
    {
      ANA_MSG_DEBUG(" Fail NFatJets with " << nfatjets);
      return false;
    }
  m_cutflow->execute(m_cf_nfatjets,m_eventWeight);

  //
  // Choose h-cand
  m_Hcand=nullptr;
  switch(m_Hcand_mode)
    {
    case Truth:
      for(uint i=0;i<m_event->fatjets();i++)
	{
	  const xAH::FatJet *fatjet=m_event->fatjet(i);

	  if(fatjet->p4.DeltaR(m_event->m_Zprime)<0.8)
	    {
	      m_Hcand=fatjet;
	      break;
	    }
	}
    default:
    case LeadJet:
      m_Hcand=m_event->fatjet(0);
      break;
    }

  if(!m_Hcand)
    {
      ANA_MSG_DEBUG(" Fail due to latch of truth match.");
      return false;
    }
  m_cutflow->execute(m_cf_match, m_eventWeight);

  return true;
}

bool HbbISRTruthHistsAlgo :: doBtagCutflow()
{ return true; }
