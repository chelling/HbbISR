import ROOT
from xAODAnaHelpers import Config
from ZprimeDM import commonconfigR20p7 as commonconfig

import itertools

isSys=False
histDetailStr   = "" #2d" #2d debug"
fatjetDetailStr= "kinematic"

c = Config()

commonsel={"m_msgLevel"               : ROOT.MSG.INFO,
           "m_mc"                     : args.is_MC,
           "m_histDetailStr"          : histDetailStr,
           "m_fatjetDetailStr"        : fatjetDetailStr,
           "m_doPUReweight"           : False,
           "m_doTrigger"              : False,
           "m_trkJet"                 : ""
           }

#
# Process Ntuple
#
c.algorithm("MiniTreeEventSelection", { "m_name"                   : "MiniTreeEventSelection",
                                        "m_msgLevel"               : ROOT.MSG.INFO,
                                        "m_mc"                     : args.is_MC,
                                        "m_applyGRL"               : False,
                                        "m_doPUreweighting"        : False,
                                        "m_doTruthOnly"            : True,
                                        "m_fatjetDetailStr"        : fatjetDetailStr
                                        } )

#
# Different selections
fjpts  =[0,250,480]
hpts   =[0,'fjpt']

for fjpt,hpt in itertools.product(fjpts,hpts):
    if hpt=='fjpt': hpt=fjpt

    name=['hbbisr_truth']
    if fjpt>0: name.append('fj%d'%fjpt)
    if hpt>0 : name.append('hpt%d'%hpt)
    #name.append('boost')
    name='_'.join(name)
    config=commonsel.copy()
    config.update({"m_name"          : name,
                   "m_Hcand_mode"    : ROOT.HbbISRTruthHistsAlgo.Truth,
                   "m_fatjet0PtCut"  : fjpt,
                   'm_HcandPtCut'    : hpt,
                   'm_doBoostCut'    : False
                   })
    c.algorithm("HbbISRTruthHistsAlgo", config )

    name=['hbbisr_leadjet']
    if fjpt>0: name.append('fj%d'%fjpt)
    if hpt>0 : name.append('hpt%d'%hpt)
    #name.append('boost')
    name='_'.join(name)
    config=commonsel.copy()
    config.update({"m_name"          : name,
                   "m_Hcand_mode"    : ROOT.HbbISRTruthHistsAlgo.LeadJet,
                   "m_fatjet0PtCut"  : fjpt,
                   'm_HcandPtCut'    : hpt,
                   'm_doBoostCut'    : False
                   })
    c.algorithm("HbbISRTruthHistsAlgo", config )
