import ROOT
from xAODAnaHelpers import Config
from ZprimeDM import commonconfig

import itertools

isSys=False
histDetailStr    =""
jetDetailStr     ="kinematic clean"
if args.is_MC: jetDetailStr+=" truth"
fatjetDetailStr  ="kinematic substructure constituent constituentAll trackJetName_GhostAntiKt2TrackJet_GhostVR30Rmax4Rmin02TrackJet VTags"
if args.is_MC: fatjetDetailStr+=" bosonCount"

nameSuffix=""
if args.treeName!="outTree":
    isSys=True

    histDetailStr  =""
    jetDetailStr   ="kinematic clean"
    photonDetailStr="kinematic"

    nameSuffix="/sys"+args.treeName[7:]
else:
   isSys=False

c = Config()

commonsel={"m_msgLevel"               : ROOT.MSG.INFO,
           "m_mc"                     : args.is_MC,
           "m_jetDetailStr"           : jetDetailStr,
           "m_fatjetDetailStr"        : fatjetDetailStr,
           "m_doPUReweight"           : False,
           "m_doTrigger"              : True
           }

#
# Process Ntuple
#
c.algorithm("MiniTreeEventSelection", { "m_name"                   : "MiniTreeEventSelection",
                                        "m_msgLevel"               : ROOT.MSG.INFO,
                                        "m_mc"                     : args.is_MC,
                                        "m_applyGRL"               : False,
                                        "m_doPUreweighting"        : False,
                                        "m_doTruthOnly"            : False,
                                        "m_triggerDetailStr"       : "passTriggers passTrigBits prescales",
                                        "m_jetDetailStr"           : jetDetailStr,
                                        "m_fatjetDetailStr"        : fatjetDetailStr
                                        } )

#
# Triggers to study
triggers=[
    #('HLT_j360','HLT_j260'),
    #('HLT_j225_gsc420_boffperf_split','HLT_j260'),
    #('HLT_j260','L1_j100'),
    #data15
    ('HLT_j360','HLT_j260'),
    ('HLT_j360_a10r_L1J100','HLT_j110'),
    ('HLT_j360_a10_lcw_sub_L1J100','HLT_j110'),
    # data16
    ('HLT_j380','HLT_j260'),
    ('HLT_j420_a10r_L1J100','HLT_j260_a10r_L1J75'),
    ('HLT_j420_a10_lcw_L1J100','HLT_j260_a10_lcw_L1J75'),
    # data17
    ('HLT_j420','HLT_j360'),
    ('HLT_j440_a10_lcw_subjes_L1J100','HLT_j260_a10_lcw_subjes_L1J75'),
    ('HLT_j460_a10_lcw_subjes_L1J100','HLT_j260_a10_lcw_subjes_L1J75'),
    ('HLT_j440_a10r_L1J100','HLT_j260_a10r_L1J75'),
    ('HLT_j460_a10r_L1J100','HLT_j260_a10r_L1J75'),
    ('HLT_j440_a10t_lcw_jes_L1J100','HLT_j260_a10t_lcw_jes_L1J75'),
    ('HLT_j460_a10t_lcw_jes_L1J100','HLT_j260_a10t_lcw_jes_L1J75'),
    ('HLT_j390_a10t_lcw_jes_30smcINF_L1J100','HLT_j260_a10t_lcw_jes_L1J75'),
    ('HLT_j420_a10t_lcw_jes_40smcINF_L1J100','HLT_j260_a10t_lcw_jes_L1J75'),
    ]

#
# Different selections
fjpts  =[0,420,440]
fjms   =[0,50,70]

#
# b-tag the two leading jets
for trigger,fjpt,fjm in itertools.product(triggers,fjpts,fjms):
    if 'smc' not in trigger[0] and (fjpt>0 or fjm>0): continue # No need for other cuts for non-mass triggers

    name=['fatjet']
    if fjpt>0:
        name.append('fjpt%d'%fjpt)
    if fjm>0:
        name.append('fjm%d'%fjm)
    name.append(trigger[0])
    namestr='_'.join(name)
    config=commonsel.copy()
    config.update({"m_name"          : namestr+'_num',
                   "m_trigger"       : trigger[0],
                   "m_refTrigger"    : trigger[1],
                   "m_trigEffDen"    : False,
                   "m_fatjet0PtCut"  : fjpt,
                   "m_fatjet0MCut"   : fjm
                   })
    c.algorithm("HbbISRFatJetHistsAlgo", config )

    config.update({"m_name"          : namestr+'_den',
                   "m_trigEffDen"    : True
                   })
    c.algorithm("HbbISRFatJetHistsAlgo", config )
