#ifndef HbbISR_HbbHists_H
#define HbbISR_HbbHists_H

#include <xAODAnaHelpers/HistogramManager.h>
#include <xAODAnaHelpers/FatJet.h>
#include <xAODAnaHelpers/Jet.h>

class HbbHists : public HistogramManager
{
public:

  HbbHists(const std::string& name, const std::string& detailStr);
  virtual ~HbbHists() ;

  virtual StatusCode initialize();

  StatusCode execute(const xAH::FatJet *Hcand, const xAH::FatJet *nonHcand, const xAH::Jet* bjet0, const xAH::Jet* bjet1, float eventWeight);
  using HistogramManager::book;    // make other overloaded version of book() to show up in subclass
  using HistogramManager::execute; // make other overloaded version of execute() to show up in subclass

private:
  //histograms
  TH2* h_nonHcandvsHcand=nullptr;
  TH2* h_Hcand_mvspt    =nullptr;

  TH1* h_dR;
  TH1* h_dRminR;
  TH1* h_dRmaxR;
};

#endif // HbbISR_HbbHists_H
