import ROOT
from xAODAnaHelpers import Config
from ZprimeDM import commonconfig
from HbbISR import selections

import itertools

isSys=args.treeName!="outTree"

if not isSys:
    histDetailStr    ="mass truthtag"
    jetDetailStr     ="kinematic clean"
    if args.is_MC: jetDetailStr+=" truth"
    fatjetDetailStr  ="kinematic substructure constituent constituentAll trackJetName_GhostVR30Rmax4Rmin02TrackJet"
    if args.is_MC: fatjetDetailStr+=" bosonCount"
    subjetDetailStr  ="kinematic flavorTag constituent %s"%(commonconfig.generate_btag_detailstr())
    muonDetailStr    ="kinematic"
else:
    histDetailStr    =""
    jetDetailStr     ="kinematic clean"
    fatjetDetailStr  ="kinematic substructure constituent constituentAll trackJetName_GhostAntiKt2TrackJet_GhostVR30Rmax4Rmin02TrackJet"
    subjetDetailStr  ="kinematic %s"%(commonconfig.generate_btag_detailstr(btaggers=['MV2c10'], btagmodes=['FixedCutBEff'], btagWPs=[85,77]))
    muonDetailStr    ="kinematic"

nameSuffix="/sys"+args.treeName[7:] if isSys else ''

c = Config()

commonsel={"m_msgLevel"               : ROOT.MSG.INFO,
           "m_mc"                     : args.is_MC,
           "m_histDetailStr"          : histDetailStr if not isSys else '',
           "m_jetDetailStr"           : jetDetailStr if not isSys else '',
           "m_fatjetDetailStr"        : fatjetDetailStr if not isSys else 'kinematic',
           "m_subjetDetailStr"        : subjetDetailStr if not isSys else '',
           "m_muonDetailStr"          : muonDetailStr if not isSys else '',
           #"m_electronDetailStr"      : electronDetailStr,
           "m_doTrigger"              : True,
           "m_trigger"                : "HLT_j360_a10_lcw_sub_L1J100,HLT_j420_a10_lcw_L1J100,HLT_j460_a10t_lcw_jes_L1J100",
           "m_doCleaning"             : True
           }

#
# Process Ntuple
#
c.algorithm("MiniTreeEventSelection", { "m_name"                   : "MiniTreeEventSelection",
                                        "m_msgLevel"               : ROOT.MSG.INFO,
                                        "m_mc"                     : args.is_MC,
                                        "m_applyGRL"               : False,
                                        "m_doPUreweighting"        : False,
                                        "m_doTruthOnly"            : False,
                                        "m_eventDetailStr"         : "pileup weightsSys",
                                        "m_triggerDetailStr"       : "passTriggers passTrigBits",
                                        "m_jetDetailStr"           : jetDetailStr,
                                        "m_fatjetDetailStr"        : fatjetDetailStr,
                                        "m_subjetDetailStr"        : subjetDetailStr,
                                        "m_muonDetailStr"          : muonDetailStr,
                                        #"m_electronDetailStr"      : electronDetailStr
                                        } )

c.algorithm("JMRUncertaintyAlgo", { "m_name"                   : "JMRUncertaintyAlgo",
                                    "m_msgLevel"               : ROOT.MSG.INFO,
                                    "m_do1Sigma"               : True,
                                    "m_do2Sigma"               : True,
                                    "m_do3Sigma"               : True
                                    } )

#
# Different selections
fjpts             =[250,480]
trkjets           =['GhostAntiKt2TrackJet','GhostVR30Rmax4Rmin02TrackJet']
skipDoubleTrkJets =[False, True]
allowSingleTrkJets=[False, True]
nbtags            =[0,1,2]
lnots             =[False, True] # Loose tag, but no tight
nbexts            =[False, True] # Count btags exactly
wps               =['None']+['{btagger}_{btagmode}_{btagWP}'.format(btagger=btagger,btagmode=btagmode,btagWP=btagWP) for btagger,btagmode,btagWP in itertools.product(commonconfig.btaggers,commonconfig.btagmodes,commonconfig.btagWPs)]
hidxs             =[-1,0,1]
hpts              =[0,'fjpt']
hms               =[0,40]
d2sorts           =[False, True]
fatjetidxs        =[0]
ttbars            =[None,'CR','veto']

# useful selections
anaselections=[]
#  trkJet, fjpt, skipDoubleTrkJet, allowSingleTrkJet, nbtag, nbext, wp, lnot, hpt, ttbar
anaselections.append(('GhostVR30Rmax4Rmin02TrackJet',480, False, False, 2,True ,'MV2c10_FixedCutBEff_77',False,'fjpt', 'veto')) # Signal region
anaselections.append(('GhostVR30Rmax4Rmin02TrackJet',480, False, False, 0,True ,'MV2c10_FixedCutBEff_85',False,'fjpt', 'veto')) # Control Region
anaselections.append(('GhostVR30Rmax4Rmin02TrackJet',480, False, False, 0,True ,'MV2c10_FixedCutBEff_77',False,'fjpt', 'veto')) # Inverted control region
anaselections.append(('GhostVR30Rmax4Rmin02TrackJet',480, False, False, 2,True ,'MV2c10_FixedCutBEff_77',True ,'fjpt', 'veto')) # Tiny Validation Region
anaselections.append(('GhostVR30Rmax4Rmin02TrackJet',480, False, False, 1,False,'MV2c10_FixedCutBEff_77',True ,'fjpt', 'veto')) # Extended Validation Region
anaselections.append(('GhostVR30Rmax4Rmin02TrackJet',480, False, False, 1,True ,'MV2c10_FixedCutBEff_77',False,'fjpt', None  )) # ttbar 1-tag region, pre muon
anaselections.append(('GhostVR30Rmax4Rmin02TrackJet',480, False, False, 0,True ,'MV2c10_FixedCutBEff_77',False,'fjpt', 'CR'  )) # ttbar 0-tag region
anaselections.append(('GhostVR30Rmax4Rmin02TrackJet',480, False, False, 1,True ,'MV2c10_FixedCutBEff_77',False,'fjpt', 'CR'  )) # ttbar 1-tag region
anaselections.append(('GhostVR30Rmax4Rmin02TrackJet',480, False, False, 2,True ,'MV2c10_FixedCutBEff_77',False,'fjpt', 'CR'  )) # ttbar 2-tag region

# Comment out the following if running "different selections" instead of useful selections
prw=True and args.is_MC
d2sort=False
fatjetidx=0
hidx=-1
hm=0
skipBoost=False
skipVRcontain=False

#
# Run the standard selection
#for trkjet,fjpt,skipDoubleTrkJet,allowSingleTrkJet,d2sort,nbtag,nbext,wp,lnot,hidx,hpt,hm,ttbar in itertools.product(trkjets,fjpts,skipDoubleTrkJets,allowSingleTrkJets,d2sorts,nbtags,nbexts,wps,lnots,hidxs,hpts,hms,ttbars):
for trkjet,fjpt,skipDoubleTrkJet,allowSingleTrkJet,nbtag,nbext,wp,lnot,hpt,ttbar in anaselections:
    selections.selection(c,
                         args.is_MC, prw,
                         trkjet, fjpt, skipDoubleTrkJet, allowSingleTrkJet, skipBoost, skipVRcontain, d2sort, fatjetidx, ttbar, hidx, hpt, hm, nbtag, nbext, wp, lnot,
                         commonsel=commonsel, nameSuffix=nameSuffix, prefix='hbbisr')

#
# Do JMR smearing
if args.is_MC and not isSys:
    jmrcommonsel=commonsel.copy()
    jmrcommonsel.update({"m_histDetailStr"    : '',
                         "m_jetDetailStr"     : '',
                         "m_fatjetDetailStr"  : 'kinematic',
                         "m_subjetDetailStr"  : '',
                         "m_muonDetailStr"    : '',
                         "m_electronDetailStr": ''
                        })

    for sigma in range(1,4):
        nameSuffix='/sysJET_MassRes__{}up'.format(sigma)
        JMRuncert=getattr(ROOT.JMRUncertaintyAlgo,'SIGMA{}'.format(sigma))
        for trkjet,fjpt,skipDoubleTrkJet,allowSingleTrkJet,nbtag,nbext,wp,lnot,hpt,ttbar in anaselections:
            selections.selection(c,
                                 args.is_MC, prw,
                                 trkjet, fjpt, skipDoubleTrkJet, allowSingleTrkJet, skipBoost, skipVRcontain, d2sort, fatjetidx, ttbar, hidx, hpt, hm, nbtag, nbext, wp, lnot,
                                 JMRuncert=JMRuncert,
                                 commonsel=jmrcommonsel, nameSuffix=nameSuffix)
