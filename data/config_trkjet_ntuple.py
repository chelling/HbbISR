import ROOT
from xAODAnaHelpers import Config, utils
if utils.is_release21():
    from ZprimeDM import commonconfig
else:
    from ZprimeDM import commonconfigR20p7 as commonconfig

c = Config()

trkjetDetailStr="kinematic flavorTag constituent %s"%(commonconfig.generate_btag_detailstr())

commonconfig.apply_common_config(c,
                                 isMC=args.is_MC,isAFII=args.is_AFII,doSyst=False,
                                 triggerSelection='HLT_j[0-9]+|HLT_j.*_a10_lcw_L1J[0-9]+|HLT_j.*_a10r_L1J[0-9]+|HLT_j.*_a10_lcw_subjes_L1J[0-9]+|HLT_j.*_a10_lcw_sub_L1J[0-9]+|HLT_j.*_a10t_lcw_jes_L1J[0-9]+|HLT_j[0-9]+_a10t_lcw_jes_[0-9]+smcINF_L1SC111|HLT_j[0-9]+_a10t_lcw_jes_[0-9]+smcINF_L1J100|HLT_j260_320eta490|HLT_j225_gsc.*_boffperf_split|HLT_3j200|HLT_4j100|HLT_ht1000_L1J100|L1_J100',
                                 doJets=False,doFatJets=False,doTrackJets=True,doPhotons=False,doMuons=False,doElectrons=False)
#btaggers=['MV2c10'],btagmodes=['FixedCutBEff'],btagWPs=[77])

containers=ROOT.vector('ZprimeNtuplerContainer')()
containers.push_back(ROOT.ZprimeNtuplerContainer(ROOT.ZprimeNtuplerContainer.JET  ,'SignalVRTrackJets' ,'vrtrkjet', trkjetDetailStr, ''))

c.algorithm("ZprimeNtupler",       {
        "m_containers"          : containers
        } )
                     
