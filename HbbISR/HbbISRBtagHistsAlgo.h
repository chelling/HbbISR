#ifndef HbbISR_HbbISRBtagHistsAlgo_H
#define HbbISR_HbbISRBtagHistsAlgo_H

// algorithm wrapper
#include <HbbISR/HbbISRHistsBaseAlgo.h>

class HbbISRBtagHistsAlgo : public HbbISRHistsBaseAlgo
{
public:
  xAH::Jet::BTaggerOP m_bTagWP; // Btagging working point. 
  float m_bTagCut; // Btagging MV2c10 cut, set m_bTagWP=None to use
  uint m_nBTagsCut; // Minimum number of b-tags to define the Higgs candidate
  uint m_nTrackJetsCut; // Minimum number of track jets

protected:
  virtual void initHcandCutflow();
  virtual void initBtagCutflow();
  virtual bool doHcandCutflow();
  virtual bool doBtagCutflow();

public:
  // this is a standard constructor
  HbbISRBtagHistsAlgo ();

private:
  //
  // cutflow
  int m_cf_nfatjets;
  int m_cf_nbtag;
  int m_cf_ntjet;

  // this is needed to distribute the algorithm to the workers
  ClassDef(HbbISRBtagHistsAlgo, 1);
};

#endif // HbbISR_HbbISRBtagHistsAlgo_H
