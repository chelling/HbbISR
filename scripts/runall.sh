#!/bin/bash

export PACKAGE=HbbISR
source ${AnalysisBase_PLATFORM}/bin/librun.sh

# sample list
SIG_GRID="../HbbISR/filelists/mc16[ad]/Signal_trijet.fatjet.NTUP.list ../HbbISR/filelists/mc16[ad]/Higgs_ggFHbb.fatjet.NTUP.list ../HbbISR/filelists/mc16[ad]/Higgs_vbfHbb.fatjet.NTUP.list ../HbbISR/filelists/mc16[ad]/Higgs_VHbb.fatjet.NTUP.list ../HbbISR/filelists/mc16[ad]/Sherpa_?qq.fatjet.NTUP.list ../HbbISR/filelists/mc16[ad]/Powheg_ttbar.fatjet.NTUP.list ../HbbISR/filelists/mc16[ad]/Powheg_singletop.fatjet.NTUP.list"
BKG_GRID="../HbbISR/filelists/mc16[ad]/Pythia8_dijet.fatjet.NTUP.list ../HbbISR/filelists/mc16[ad]/Herwig_?qq.fatjet.NTUP.list ../HbbISR/filelists/mc16[ad]/Sherpa225_?qq.fatjet.NTUP.list ../HbbISR/filelists/mc16[ad]/Sherpa_ttbar.fatjet.NTUP.list ../HbbISR/filelists/mc16[ad]/Sherpa_W*nu.fatjet.NTUP.list"
DATA_GRID="../HbbISR/filelists/data15.fatjet.NTUP.list ../HbbISR/filelists/data16.fatjet.NTUP.list ../HbbISR/filelists/data17.fatjet.NTUP.list"

syslist=(JET_Comb_Baseline_All JET_Comb_Modelling_All JET_Comb_TotalStat_All JET_Comb_Tracking_All)
varlist=(1up 2up 3up 1down 2down 3down)
combsyslist=()

# Process
RUNLIST=""

# fatjet
runOne fatjet fatjet_mc_test_nominal "-m" ${SIG_GRID} ${BKG_GRID}
for sys in ${syslist[@]}
do
    for var in ${varlist[@]}
    do
  	sysname="${sys}__${var}"
  	runOne fatjet fatjet_mc_test_sys${sysname} "-m -s ${sysname}" ${SIG_GRID}
 	combsyslist+=(${sysname})
    done
done

runOne fatjet fatjet_data_test "" ${DATA_GRID}

echo "Waiting..."
wait

#
# Merge
#

# fatjet
mergeSys fatjet_mc_test ${combsyslist[@]}
mergemc fatjet_mc_test qcd.root "*Pythia*jetjet*JZ*[0-9]WithSW.*.root"
mergemc fatjet_mc_test ttbar.root "*PhPy8EG_A14_ttbar_hdamp258p75*.root"
mergemc fatjet_mc_test ttbar-Sherpa.root "*Sherpa_221_NNPDF30NNLO_ttbar*.root"
mergemc fatjet_mc_test singletop.root "*PowhegPythia8EvtGen_A14_Wt*.root" #"*PhPy8EG_A14_tchan*.root"
mergemc fatjet_mc_test w.root "*Sherpa_CT10_Wqq*.root"
mergemc fatjet_mc_test z.root "*Sherpa_CT10_Zqq*.root"
mergemc fatjet_mc_test v.root "*Sherpa_CT10_*qq*.root"
mergemc fatjet_mc_test w-Herwig.root "*Herwigpp_UEEE5CTEQ6L1_Wj*.root"
mergemc fatjet_mc_test z-Herwig.root "*Herwigpp_UEEE5CTEQ6L1_Zj*.root"
mergemc fatjet_mc_test v-Herwig.root "*Herwigpp_UEEE5CTEQ6L1_*j*.root"
mergemc fatjet_mc_test w-Sherpa225.root "*Sherpa_225_NNPDF30NNLO_Wqq*.root"
mergemc fatjet_mc_test z-Sherpa225.root "*Sherpa_225_NNPDF30NNLO_Zqq*.root"
mergemc fatjet_mc_test v-Sherpa225.root "*Sherpa_225_NNPDF30NNLO_*qq*.root"
mergemc fatjet_mc_test sig.root "*PowhegPy8EG_NNLOPS_nnlo_30_ggH125_bb_kt200*.root" "*345931*.PowhegPy8EG_NNPDF30_AZNLOCTEQ6L1_VBFH125_bb*.root" "*.Pythia8EvtGen_A14NNPDF23LO_*H125_bb_fj350*.root"
for i in mRp1 mRp125 mRp15 mRp175 mRp2 mRp25 mRp3
do
    mergemc fatjet_mc_test MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350P100_${i}_gSp25.root "*.MGPy8EG_N30LO_A14N23LO_dmA_*j_Jet350P100_${i}_gSp25.*.root"
done 
mergemc fatjet_mc_test Wlnu.root "*_W*nu*.root"
mergemc fatjet_mc_test Wenu.root   "*Wenu*.root"
mergemc fatjet_mc_test Wmunu.root  "*Wmunu*.root"
mergemc fatjet_mc_test Wtaunu.root "*Wtaunu*.root"

merge fatjet_data_test data15.root "*data15*root"
merge fatjet_data_test data16.root "*data16*root"
merge fatjet_data_test data17.root "*data17*root"
merge fatjet_data_test data.root "data*.root"
merge fatjet_data_test data1516.root "data1[56]*.root"
