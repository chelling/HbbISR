#include <HbbISR/HbbHists.h>

HbbHists :: HbbHists (const std::string& name, const std::string& detailStr)
  : HistogramManager(name, detailStr)
{ }

HbbHists :: ~HbbHists () 
{ }

StatusCode HbbHists::initialize()
{
  h_nonHcandvsHcand = book(m_name, "nonHcandvsHcand",
			   "Signal Candidate Mass [GeV]"    , 40, 0, 200,
			   "Non Signal Candidate Mass [GeV]", 40, 0, 200);

  h_Hcand_mvspt = book(m_name, "Hcand_mvspt",
		       "Signal Candidate p_{T} [GeV]" ,200, 0, 1000,
		       "Signal Candidate Mass [GeV]"  , 40, 0,  200);

  h_dR     = book(m_name, "dR"    , "#DeltaR_{bb}", 100, 0, 2);
  h_dRminR = book(m_name, "dRminR", "#DeltaR_{bb}/min(R_{b})", 100, 0, 5);
  h_dRmaxR = book(m_name, "dRmaxR", "#DeltaR_{bb}/max(R_{b})", 100, 0, 5);

  return StatusCode::SUCCESS;
}

StatusCode HbbHists::execute(const xAH::FatJet *Hcand, const xAH::FatJet *nonHcand, const xAH::Jet* bjet0, const xAH::Jet* bjet1, float eventWeight)
{
  ANA_CHECK(HistogramManager::execute());

  h_nonHcandvsHcand->Fill(Hcand->p4.M(), (nonHcand)?nonHcand->p4.M():0, eventWeight);
  h_Hcand_mvspt->Fill(Hcand->p4.Pt(), Hcand->p4.M(), eventWeight);

  float R0=std::max(0.02, std::min(0.4, 30/bjet0->p4.Pt()));
  float R1=std::max(0.02, std::min(0.4, 30/bjet1->p4.Pt()));
  float minR=std::min(R0,R1);
  float maxR=std::max(R0,R1);

  float dR=bjet0->p4.DeltaR(bjet1->p4);

  h_dR->Fill(dR, eventWeight);
  h_dRminR->Fill(dR/minR, eventWeight);
  h_dRmaxR->Fill(dR/maxR, eventWeight);

  return StatusCode::SUCCESS;
}
