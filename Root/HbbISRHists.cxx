#include <HbbISR/HbbISRHists.h>

#include <sstream>

HbbISRHists :: HbbISRHists (const std::string& name, const std::string& detailStr, const std::string& jetDetailStr, const std::string& muonDetailStr, const std::string& electronDetailStr, const std::string& fatjetDetailStr, const std::string& trkJet, const std::string& subjetDetailStr)
  : EventHists(name, detailStr),
    m_detailStr(detailStr), m_vbf(nullptr), m_hbb(nullptr),
    m_jetDetailStr(jetDetailStr), h_nJet(nullptr), m_jet0(nullptr), m_jet1(nullptr), m_jet2(nullptr), m_jet3(nullptr),
    m_muonDetailStr(muonDetailStr), h_nMuon(nullptr), m_muon0(nullptr), m_muon1(nullptr), m_muon2(nullptr),
    m_electronDetailStr(electronDetailStr), h_nElectron(nullptr), m_electron0(nullptr), m_electron1(nullptr), m_electron2(nullptr),
    m_fatjetDetailStr(fatjetDetailStr),
    m_trkJet(trkJet), m_subjetDetailStr(subjetDetailStr), h_nTrkJet(nullptr), m_trkjet0(nullptr), m_trkjet1(nullptr), m_trkjet2(nullptr)
{ }

HbbISRHists :: ~HbbISRHists () 
{ }

void HbbISRHists::record(EL::IWorker* wk)
{
  EventHists::record(wk);

  m_vbf         ->record(wk);
  m_hbb         ->record(wk);

  if(!m_jetDetailStr.empty())
    {
      m_jet0     ->record(wk);
      m_jet1     ->record(wk);
      m_jet2     ->record(wk);
      m_jet3     ->record(wk);
    }

  if(!m_muonDetailStr.empty())
    {
      m_muon0    ->record(wk);
      m_muon1    ->record(wk);
      m_muon2    ->record(wk);
    }

  if(!m_electronDetailStr.empty())
    {
      m_electron0->record(wk);
      m_electron1->record(wk);
      m_electron2->record(wk);
    }

  if(!m_fatjetDetailStr.empty())
    {
      m_Hcand    ->record(wk);
      m_nonHcand ->record(wk);
      m_fatjet0  ->record(wk);
      m_fatjet1  ->record(wk);
      m_fatjet2  ->record(wk);
    }

  if(!m_subjetDetailStr.empty())
    {
      m_trkjet0  ->record(wk);
      m_trkjet1  ->record(wk);
      m_trkjet2  ->record(wk);
    }
}

StatusCode HbbISRHists::initialize()
{
  if(m_debug) std::cout << "HbbISRHists::initialize()" << std::endl;

  ANA_CHECK(EventHists::initialize());

  m_vbf          =new ZprimeResonanceHists(m_name+"vbf_", m_detailStr);
  ANA_CHECK(m_vbf->initialize());

  m_hbb          =new HbbHists(m_name+"hbb_", "");
  ANA_CHECK(m_hbb->initialize());

  h_HcandIdx     = book(m_name, "HcandIdx", "Signal Candidate Index", 6, -1.5, 4.5 );

  h_Zprime_pt    = book(m_name, "Zprime_pt", "Z' p_{T} [GeV]", 100, 0, 1000);

  if(!m_jetDetailStr.empty())
    {
      h_nJet     = book(m_name, "nJets", "N_{jets}", 10, -0.5, 9.5 );

      m_jet0=new ZprimeDM::JetHists(m_name+"jet0_" , m_jetDetailStr, "leading");
      ANA_CHECK(m_jet0->initialize());

      m_jet1=new ZprimeDM::JetHists(m_name+"jet1_" , m_jetDetailStr, "subleading");
      ANA_CHECK(m_jet1->initialize());

      m_jet2=new ZprimeDM::JetHists(m_name+"jet2_" , m_jetDetailStr, "third");
      ANA_CHECK(m_jet2->initialize());

      m_jet3=new ZprimeDM::JetHists(m_name+"jet3_" , m_jetDetailStr, "fourth");
      ANA_CHECK(m_jet3->initialize());
    }

  if(!m_muonDetailStr.empty())
    {
      h_nMuon     = book(m_name, "nMuons", "N_{#mu}", 10, -0.5, 9.5 );

      m_muon0=new ZprimeDM::MuonHists(m_name+"muon0_" , m_muonDetailStr, "leading");
      ANA_CHECK(m_muon0->initialize());

      m_muon1=new ZprimeDM::MuonHists(m_name+"muon1_" , m_muonDetailStr, "subleading");
      ANA_CHECK(m_muon1->initialize());

      m_muon2=new ZprimeDM::MuonHists(m_name+"muon2_" , m_muonDetailStr, "third");
      ANA_CHECK(m_muon2->initialize());
    }

  if(!m_electronDetailStr.empty())
    {
      h_nElectron= book(m_name, "nElectrons", "N_{el}", 10, -0.5, 9.5 );

      m_electron0=new ZprimeDM::ElectronHists(m_name+"el0_" , m_electronDetailStr, "leading");
      ANA_CHECK(m_electron0->initialize());

      m_electron1=new ZprimeDM::ElectronHists(m_name+"el1_" , m_electronDetailStr, "subleading");
      ANA_CHECK(m_electron1->initialize());

      m_electron2=new ZprimeDM::ElectronHists(m_name+"el2_" , m_electronDetailStr, "third");
      ANA_CHECK(m_electron2->initialize());
    }

  if(!m_fatjetDetailStr.empty())
    {
      h_nFatJet  = book(m_name, "nFatJets",  "N_{fatjet}",    10,     -0.5,     9.5 );

      m_Hcand    =new ZprimeDM::FatJetHists(m_name+"Hcand_"   , m_fatjetDetailStr, "signal candidate");
      ANA_CHECK(m_Hcand   ->initialize());

      m_nonHcand =new ZprimeDM::FatJetHists(m_name+"nonHcand_", m_fatjetDetailStr, "secondary signal candidate");
      ANA_CHECK(m_nonHcand->initialize());

      m_fatjet0  =new ZprimeDM::FatJetHists(m_name+"fatjet0_" , m_fatjetDetailStr, "leading");
      ANA_CHECK(m_fatjet0 ->initialize());

      m_fatjet1  =new ZprimeDM::FatJetHists(m_name+"fatjet1_" , m_fatjetDetailStr, "subleading");
      ANA_CHECK(m_fatjet1 ->initialize());

      m_fatjet2  =new ZprimeDM::FatJetHists(m_name+"fatjet2_" , m_fatjetDetailStr, "third");
      ANA_CHECK(m_fatjet2 ->initialize());
    }

  if(!m_subjetDetailStr.empty())
    {
      h_nTrkJet  = book(m_name, "nTrkJets",  "N_{trkjet}",    10,     -0.5,     9.5 );

      m_trkjet0  =new ZprimeDM::JetHists(m_name+"trkjet0_", m_subjetDetailStr, "leading Hcand trk");
      ANA_CHECK(m_trkjet0->initialize());

      m_trkjet1  =new ZprimeDM::JetHists(m_name+"trkjet1_", m_subjetDetailStr, "subleading Hcand trk");
      ANA_CHECK(m_trkjet1->initialize());

      m_trkjet2  =new ZprimeDM::JetHists(m_name+"trkjet2_", m_subjetDetailStr, "third Hcand trk");
      ANA_CHECK(m_trkjet2->initialize());
    }

  //
  // Muons relative to the Hcand, used for ttbarCR
  h_Hcandmuon0_dPhi= book(m_name, "Hcandmuon0_dPhi", "#Delta#phi_{signal candidate index,leading muon}", 10, 0, TMath::Pi());

  return StatusCode::SUCCESS;
}

StatusCode HbbISRHists::execute(const DijetISREvent& event, const xAH::FatJet *Hcand, const xAH::FatJet *nonHcand, float eventWeight)
{
  if(m_debug) std::cout << "HbbISRHists::execute()" << std::endl;
  ANA_CHECK(EventHists::execute(event, eventWeight));

  //
  // Jets
  if(!m_jetDetailStr.empty())
    {
      h_nJet   ->Fill(event.jets   (), eventWeight);

      if(event.jets     ()>0) ANA_CHECK(m_jet0     ->execute(event.jet(0)   , eventWeight));
      if(event.jets     ()>1) ANA_CHECK(m_jet1     ->execute(event.jet(1)   , eventWeight));
      if(event.jets     ()>2) ANA_CHECK(m_jet2     ->execute(event.jet(2)   , eventWeight));
      if(event.jets     ()>3) ANA_CHECK(m_jet3     ->execute(event.jet(3)   , eventWeight));
    }

  //
  // Muons
  if(!m_muonDetailStr.empty())
    {
      h_nMuon   ->Fill(event.muons   (), eventWeight);

      if(event.muons    ()>0) ANA_CHECK(m_muon0    ->execute(event.muon(0)   , eventWeight));
      if(event.muons    ()>1) ANA_CHECK(m_muon1    ->execute(event.muon(1)   , eventWeight));
      if(event.muons    ()>2) ANA_CHECK(m_muon2    ->execute(event.muon(2)   , eventWeight));
    }

  //
  // Electrons
  if(!m_electronDetailStr.empty())
    {
      h_nElectron   ->Fill(event.electrons   (), eventWeight);

      if(event.electrons()>0) ANA_CHECK(m_electron0->execute(event.electron(0), eventWeight));
      if(event.electrons()>1) ANA_CHECK(m_electron1->execute(event.electron(1), eventWeight));
      if(event.electrons()>2) ANA_CHECK(m_electron2->execute(event.electron(2), eventWeight));
    }

  //
  // Fat jets
  if(!m_fatjetDetailStr.empty())
    {
      h_nFatJet->Fill(event.fatjets(), eventWeight);

      if(Hcand              ) ANA_CHECK(m_Hcand    ->execute(Hcand            , eventWeight));
      if(nonHcand           ) ANA_CHECK(m_nonHcand ->execute(nonHcand         , eventWeight));
      if(event.fatjets  ()>0) ANA_CHECK(m_fatjet0  ->execute(event.fatjet(0)  , eventWeight));
      if(event.fatjets  ()>1) ANA_CHECK(m_fatjet1  ->execute(event.fatjet(1)  , eventWeight));
      if(event.fatjets  ()>2) ANA_CHECK(m_fatjet2  ->execute(event.fatjet(2)  , eventWeight));
    }

  //
  // Zprime kinematics
  h_Zprime_pt->Fill(event.m_Zprime_pt, eventWeight);

  //
  // Hcand trk jets
  if(Hcand!=nullptr)
    {
      if(m_trkJet!="")
	{
	  const std::vector<xAH::Jet> &trkJets=Hcand->trkJets.at(m_trkJet);

	  if(trkJets.size()>=2)
	    ANA_CHECK(m_hbb->execute(Hcand, nonHcand, &trkJets.at(0), &trkJets.at(1), eventWeight));

	  if(!m_subjetDetailStr.empty())
	    {
	      h_nTrkJet->Fill(trkJets.size(), eventWeight);

	      if(trkJets.size    ()>0) ANA_CHECK(m_trkjet0  ->execute(&trkJets.at(0)  , eventWeight));
	      if(trkJets.size    ()>1) ANA_CHECK(m_trkjet1  ->execute(&trkJets.at(1)  , eventWeight));
	      if(trkJets.size    ()>2) ANA_CHECK(m_trkjet2  ->execute(&trkJets.at(2)  , eventWeight));
	    }
	}

      // Jet position
      for(uint i=0;i<event.fatjets();i++)
	{
	  if(event.fatjet(i)==Hcand) 
	    {
	      h_HcandIdx->Fill(i, eventWeight);
	      break;
	    }
	}

      // VBF jets
      const xAH::Jet *vbf0=nullptr;
      const xAH::Jet *vbf1=nullptr;
      for(uint i=0; i<event.jets(); i++)
	{
	  const xAH::Jet *vbfcand=event.jet(i);
	  if(Hcand->p4.DeltaR(vbfcand->p4)>1.)
	    {
	      if(vbf0==0) vbf0=vbfcand;
	      else vbf1=vbfcand;
	      if(vbf1!=0) break;
	    }
	}
      if(vbf0 && vbf1)
	ANA_CHECK(m_vbf->execute(vbf0, vbf1, eventWeight));
    }
  else
    {
      h_HcandIdx->Fill(-1, eventWeight);
    }

  // Hcandmuon
  if(Hcand!=nullptr)
    {
      if(event.muons()>0) h_Hcandmuon0_dPhi->Fill(fabsf(Hcand->p4.DeltaPhi(event.muon(0)->p4)) ,eventWeight);
    }

  return StatusCode::SUCCESS;
}

StatusCode HbbISRHists::finalize()
{
  if(m_debug) std::cout << "HbbISRHists::finalize()" << std::endl;
  
  ANA_CHECK(EventHists::finalize());

  ANA_CHECK(m_vbf->finalize());
  delete m_vbf;

  ANA_CHECK(m_hbb->finalize());
  delete m_hbb;

  //
  // Jets
  if(!m_jetDetailStr.empty())
    {
      ANA_CHECK(m_jet0   ->finalize());
      ANA_CHECK(m_jet1   ->finalize());
      ANA_CHECK(m_jet2   ->finalize());
      ANA_CHECK(m_jet3   ->finalize());

      delete m_jet0;
      delete m_jet1;
      delete m_jet2;
      delete m_jet3;
    }

  //
  // Muons
  if(!m_muonDetailStr.empty())
    {
      ANA_CHECK(m_muon0   ->finalize());
      ANA_CHECK(m_muon1   ->finalize());
      ANA_CHECK(m_muon2   ->finalize());

      delete m_muon0;
      delete m_muon1;
      delete m_muon2;
    }

  //
  // Electrons
  if(!m_electronDetailStr.empty())
    {
      ANA_CHECK(m_electron0   ->finalize());
      ANA_CHECK(m_electron1   ->finalize());
      ANA_CHECK(m_electron2   ->finalize());

      delete m_electron0;
      delete m_electron1;
      delete m_electron2;
    }

  //
  // Fat Jets
  if(!m_fatjetDetailStr.empty())
    {
      ANA_CHECK(m_Hcand   ->finalize());
      ANA_CHECK(m_fatjet0 ->finalize());
      ANA_CHECK(m_fatjet1 ->finalize());
      ANA_CHECK(m_fatjet2 ->finalize());

      delete m_Hcand;
      delete m_fatjet0;
      delete m_fatjet1;
      delete m_fatjet2;
    }

  return StatusCode::SUCCESS;
}
