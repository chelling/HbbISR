#!/usr/bin/env python

import sys
import time
import tempfile
import getpass
import subprocess
import os
import argparse

from ZprimeDM import ntupler

##-----------------------------------------------##
## Users: set sample information here

#
# Mapping of different options to run. Each option has
# the following attributes:
#  selection: name of ntupler configuration to run (SELECTION from config_SELECTION_ntuple.py)
#  filelists_type: list of filelists (FILELIST from FILELIST.deriv.list) for type
#                  type can be any one of data, bkg, sig or fastsim
#  derivation: name of derivation to use for the filelists (DERIV from filelist.DERIV.list)
#  
# Notes: 
#  * p3308 data
#  * p3309 mc
optionDict = {
   # "test" :  {'selection': 'fatjet',
   #            'derivation':'EXOT8',
   #            'filelists_bkg':[#"gridTest.mc",
   #                             ],
   #            'filelists_data':[#"gridTest.data",
   #                              ]
   #            },

    "fatjet" :  {'selection': 'fatjet',
                 'derivation':'EXOT8',
                 'filelists_bkg':["Pythia8_dijet",
                                  "Sherpa_ttbar",
                                  "Herwig_Wqq",
                                  "Herwig_Zqq"
                                  ],
                 'filelists_sig':["Powheg_ttbar",
                                  "Powheg_singletop",
                                  "Sherpa225_Wqq",
                                  "Sherpa225_Zqq",
                                  "Higgs_VHbb",
                                  "Higgs_ggFHbb",
                                  "Higgs_vbfHbb",
                                  "Signal_trijet",
                                  "Sherpa_Wenu",
                                  "Sherpa_Wmunu",
                                  "Sherpa_Wtaunu"
                                  ],
                 'filelists_data':["data15",
                                   "data16",
                                   "data17",
                                   "data18"
                                   ]
                 },
    "trkjet" :  {'selection': 'trkjet',
                 'derivation':'EXOT8',
                 'filelists_bkg':["Sherpa_Wqq",
                                  "Sherpa_Zqq"
                                  ],
                 }
    }

## End of user specified information.

#### RUN EVERYTHING ####
ntupler.runntupler(optionDict,package='HbbISR')
